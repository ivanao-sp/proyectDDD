-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 30-05-2018 a las 06:31:55
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `myBD`
--
CREATE DATABASE IF NOT EXISTS `myBD` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `myBD`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`) VALUES
(3, 'platos'),
(4, 'tapers'),
(1, 'tazas'),
(2, 'vasos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

DROP TABLE IF EXISTS `estado`;
CREATE TABLE `estado` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`id`, `nombre`) VALUES
(2, 'Aceptado'),
(1, 'Propuesto'),
(4, 'Retirado'),
(3, 'Sin existencias');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

DROP TABLE IF EXISTS `evento`;
CREATE TABLE `evento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `descripcion` text CHARACTER SET latin1 NOT NULL,
  `rutaImagen` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `evento`
--

INSERT INTO `evento` (`id`, `nombre`, `descripcion`, `rutaImagen`) VALUES
(1, 'Excusión por monte', 'Ruta de excusión por el monte', '../ima/eventos/evento1.jpeg'),
(2, 'Esqui', 'Esqui en la montaña', '../ima/eventos/evento2.jpeg'),
(3, 'Fiesta DJ', 'fiesta con un dj', '../ima/eventos/evento3.jpg'),
(4, 'Fiesta de disfraces', 'una buena fiesta de disfraces', '../ima/eventos/evento4.jpeg'),
(5, 'Aventura en Montaña', 'Una buena aventura por la montaña', '../ima/eventos/evento5.jpg'),
(6, 'Paseo por la nieve', 'un buen paseo por la nieve', '../ima/eventos/evento6.jpg'),
(7, 'Ir a ver el atardecer', 'un bonito atardecer', '../ima/eventos/evento7.jpg'),
(8, 'Concierto Benefico', 'Un grán concierto benefico', '../ima/eventos/evento8.jpg'),
(9, 'paseo por el campo', 'un buen paseo', '../ima/eventos/evento9.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventoLugar`
--

DROP TABLE IF EXISTS `eventoLugar`;
CREATE TABLE `eventoLugar` (
  `idEventoLugar` int(11) NOT NULL,
  `idEvento` int(11) NOT NULL,
  `idLugar` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `aforo` int(11) NOT NULL,
  `precioEntrada` float NOT NULL,
  `entradasDisponibles` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `eventoLugar`
--

INSERT INTO `eventoLugar` (`idEventoLugar`, `idEvento`, `idLugar`, `fecha`, `aforo`, `precioEntrada`, `entradasDisponibles`) VALUES
(1, 1, 1, '2018-10-24', 100, 50.5, 62),
(2, 2, 2, NULL, 50, 80.99, 40),
(3, 1, 1, NULL, 80, 34.88, 70),
(4, 5, 15, '2019-01-09', 200, 50, 136),
(5, 1, 6, '2019-07-18', 250, 20, 200),
(6, 6, 3, '2019-12-26', 300, 70, 250),
(7, 3, 9, '2019-01-02', 2000, 100, 1496),
(8, 3, 5, NULL, 350, 100, 300),
(9, 7, 2, '2018-12-20', 2000, 50, 990),
(10, 4, 12, '2018-12-19', 5000, 200, 3990),
(11, 8, 1, '2018-12-19', 10000, 200, 9000),
(12, 2, 14, '2019-01-24', 200, 100, 180),
(13, 6, 9, NULL, 200, 50, 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventoParticipante`
--

DROP TABLE IF EXISTS `eventoParticipante`;
CREATE TABLE `eventoParticipante` (
  `idEvento` int(11) NOT NULL,
  `idParticipante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `eventoParticipante`
--

INSERT INTO `eventoParticipante` (`idEvento`, `idParticipante`) VALUES
(1, 4),
(1, 13),
(1, 17),
(1, 22),
(2, 1),
(2, 4),
(2, 13),
(2, 24),
(3, 1),
(3, 3),
(3, 18),
(3, 19),
(3, 21),
(3, 23),
(3, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fechasPosibles`
--

DROP TABLE IF EXISTS `fechasPosibles`;
CREATE TABLE `fechasPosibles` (
  `idEvento` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fechasPosibles`
--

INSERT INTO `fechasPosibles` (`idEvento`, `fecha`) VALUES
(1, '2018-05-15'),
(1, '2018-05-24'),
(1, '2018-06-20'),
(1, '2018-09-19'),
(1, '2018-10-24'),
(1, '2019-02-18'),
(2, '2018-09-18'),
(2, '2018-09-24'),
(2, '2018-09-28'),
(3, '2018-08-23'),
(3, '2019-09-20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

DROP TABLE IF EXISTS `grupo`;
CREATE TABLE `grupo` (
  `id` int(11) NOT NULL,
  `CIF` varchar(9) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `domicilioFiscal` varchar(45) CHARACTER SET latin1 NOT NULL,
  `web` varchar(60) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`id`, `CIF`, `nombre`, `domicilioFiscal`, `web`) VALUES
(1, '00000000C', 'Fiesteros Anonimos', 'calle A Saber Nº22', 'www.webfalsa.com'),
(2, '00000000D', 'Los Romanticos', 'calle Romantica Nº4', 'www.otrawebfalsa.com'),
(3, '00000000K', 'Los Bailaores', 'calle Inventada Nº 27', 'www.webinventada.com'),
(4, '00000000G', 'Tulipanes Verdes', 'calle Desengaño Nº 35', 'www.lostulipaneros.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenesProd`
--

DROP TABLE IF EXISTS `imagenesProd`;
CREATE TABLE `imagenesProd` (
  `id` int(11) NOT NULL,
  `idProd` int(11) NOT NULL,
  `ruta` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `imagenesProd`
--

INSERT INTO `imagenesProd` (`id`, `idProd`, `ruta`) VALUES
(1, 1, '../ima/productos/tazas/taza5/ima1.jpg'),
(2, 1, '../ima/productos/tazas/taza5/ima2.jpg'),
(3, 2, '../ima/productos/tazas/taza6/ima1.jpg'),
(4, 2, '../ima/productos/tazas/taza6/ima2.jpg'),
(5, 1, '../ima/productos/tazas/taza5/ima3.jpg'),
(6, 3, '../ima/productos/vasos/vaso1/ima1.jpg'),
(7, 3, '../ima/productos/vasos/vaso1/ima2.jpg'),
(12, 16, '../ima/productos/vasos/vaso2/ima1.jpg'),
(13, 16, '../ima/productos/vasos/vaso2/ima2.jpg'),
(14, 17, '../ima/productos/vasos/vaso3/ima1.jpg'),
(15, 17, '../ima/productos/vasos/vaso3/ima2.jpg'),
(16, 18, '../ima/productos/platos/plato1/ima1.jpg'),
(17, 18, '../ima/productos/platos/plato1/ima2.jpg'),
(18, 32, '../ima/productos/tapers/taper1/ima1.jpg'),
(19, 32, '../ima/productos/tapers/taper1/ima2.jpg'),
(20, 32, '../ima/productos/tapers/taper1/ima3.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar`
--

DROP TABLE IF EXISTS `lugar`;
CREATE TABLE `lugar` (
  `id` int(11) NOT NULL,
  `lugar` varchar(60) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lugar`
--

INSERT INTO `lugar` (`id`, `lugar`) VALUES
(5, 'Cabo Figueirido'),
(9, 'Costa Doce'),
(15, 'Estepa do Xurelo'),
(2, 'Estepas del Canguro'),
(4, 'Monque Negro'),
(14, 'Montaña de los Lobos'),
(6, 'Montaña Norte'),
(10, 'Montaña Verde'),
(12, 'Monte Bonito'),
(1, 'Paseo de Las Golondrinas'),
(3, 'Picos Azules'),
(13, 'Playa de los Olivos'),
(11, 'Rincon Del hipopotamo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participante`
--

DROP TABLE IF EXISTS `participante`;
CREATE TABLE `participante` (
  `id` int(11) NOT NULL,
  `NIF` varchar(9) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `apellido1` varchar(45) NOT NULL,
  `apellido2` varchar(45) NOT NULL,
  `email` varchar(60) CHARACTER SET latin1 NOT NULL,
  `telefono` varchar(15) CHARACTER SET latin1 NOT NULL,
  `direccion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `localidad` varchar(45) CHARACTER SET latin1 NOT NULL,
  `provincia` varchar(45) CHARACTER SET latin1 NOT NULL,
  `grupo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `participante`
--

INSERT INTO `participante` (`id`, `NIF`, `nombre`, `apellido1`, `apellido2`, `email`, `telefono`, `direccion`, `localidad`, `provincia`, `grupo`) VALUES
(1, '00000000A', 'Carlos', 'Rodriguez', 'Rodriguez', 'mailfalso@gmail.com', '600000000', 'calle Sinnombre Nº44', 'Madrid', 'Madrid', NULL),
(2, '00000000F', 'Fermin', 'Trujillo', 'Trujillo', 'otromailfalso@gmail.com', '600000000', 'calle Norte Nº34', 'A Coruña', 'A Coruña', 1),
(3, '00000000H', 'Jorge', 'Gonzalez', 'Perez', 'nuevoemailfalso@gmail.com', '700000000', 'calle Molinos Nª2', 'Gijon', 'Asturias', NULL),
(4, '00000000L', 'Enrique', 'Pastor', 'Perez', 'unnuevoemailinventado@gmail.com', '600000000', 'calle Falsa Nº 72', 'Oviedo', 'Asturias', 4),
(5, '00000000M', 'Homer', 'Rodriguez', 'Alonso', 'elotroemailinventado@gmail.com', '700000000', 'calle Mondoñedo Nº8', 'Leon', 'Leon', 3),
(12, '00000000P', 'Silvia', 'Alonso', 'Rodriguez', 'puesnoseqeponer@gmail.com', '700000000', 'calle Rocal Nº 54', 'Ponferrada', 'Leon', 1),
(13, '00000000F', 'Graciela', 'Martinez', 'Rodriguez', 'emailfalso23@gmail.com', '700000000', 'calle Tranvia Nº92', 'Gijon', 'Asturias', 4),
(14, '00000000U', 'Blanca', 'Alonso', 'Gomez', 'noseqeponer@gmail.com', '600000000', 'calle Nostalgica', 'Lugo', 'Lugo', 3),
(15, '00000000Z', 'Bart', 'Perez', 'Rodriguez', 'mailfalso4@gmail.com', '700000000', 'calle Garrido Nº4', 'Vigo', 'Pontevedra', 4),
(16, '00000000B', 'Noa', 'Estevez', 'Estevez', 'unemailinventado8@gmail.com', '600000000', 'calle Manzano Nº73', 'Madrid', 'Madrid', 3),
(17, '00000000Q', 'Manolo', 'Perez', 'Gomez', 'otromailfalso99@gmail.com', '700000000', 'calle Nose Nª52', 'Oviedo', 'Asturias', 4),
(18, '00000000Z', 'Laura', 'Garrido', 'Perez', 'mailinbventado44@gmail.com', '700000000', 'calle Vermudez Nª33', 'Madrid', 'Madrid', NULL),
(19, '00000000N', 'Noa', 'Rodriguez', 'Rodriguez', 'nuevomailfalso44@gmail.com', '700000000', 'calle Olvidada', 'Gijon', 'Asturias', NULL),
(20, '00000000D', 'Tony', 'Garrido', 'Perez', 'mailfalsoinventado32@gmail.com', '600000000', 'calle Monforte Nº4', 'Oviedo', 'Asturias', NULL),
(21, '00000000L', 'Aaron', 'Rodriguez', 'Alonso', 'otromailfalso22@gmail.com', '600000000', 'calle Castiñeiro Nº7', 'A Coruña', 'A Coruña', 4),
(22, '00000000C', 'Gonzalo', 'Perez', 'Alvarez', 'nuevoemailfalso77@gmail.com', '600000000', 'calle Perdida Nª2', 'Madrid', 'Madrid', NULL),
(23, '00000000D', 'Mariano', 'Gonzalez', 'Rodriguez', 'mailfalso442@gmail.com', '700000000', 'calle Troncoso Nº44', 'Oviedo', 'Oviedo', 2),
(24, '00000000V', 'Macarena', 'Perez', 'Rodriguez', 'nuevoemailfalso555@gmail.com', '700000000', 'calle Castaño Nº3', 'Aviles', 'Asturias', NULL),
(25, '00000000F', 'Roberto', 'Rodriguez', 'Rodriguez', 'otromailfalso92@gmail.com', '600000000', 'calle manzano Nº 4', 'Madrid', 'Madrid', 1),
(26, '00000000D', 'Carla', 'Gonzales', 'Perez', 'mailfalso200@gmail.com', '700000000', 'calle Cordoba Nº9', 'Gijon', 'Asturias', NULL),
(27, '00000000G', 'Ernesto', 'Martinez', 'Martinez', 'nuevomailfalso400000@gmail.com', '700000000', 'calle Nueva Nª79', 'Gijon', 'Asturias', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `descripcion` text CHARACTER SET latin1 NOT NULL,
  `categoria` int(11) NOT NULL,
  `fechaFinCampanha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `nombre`, `descripcion`, `categoria`, `fechaFinCampanha`) VALUES
(1, 'taza verde', 'taza verde sobre fondo beis', 1, '2019-04-08'),
(2, 'taza blanca', 'taza blanca grande', 1, '2042-03-22'),
(3, 'vaso ancho', 'vaso para cualquier bebida', 2, '2019-05-14'),
(16, 'vaso tubo', 'buen vaso', 2, '2020-07-22'),
(17, 'vaso normal', 'es un vaso', 2, '2019-04-18'),
(18, 'plato ancho', 'un gran plato', 3, '2018-05-24'),
(32, 'taper verde', 'buen taper verde', 4, '2018-12-14'),
(33, 'hhh', 'jjjj', 3, NULL),
(34, 'ffffffffff', 'kkkkkkkkkk', 4, NULL),
(35, 'tttttt', 'gggggggggg', 1, NULL),
(36, 'pruebaaaaaaaaa1', 'pruebaaaaaaaaaaaa', 4, '2034-02-14'),
(37, 'fffffffffffffffffffffffff', 'hydthrs', 1, '2019-02-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productoTienda`
--

DROP TABLE IF EXISTS `productoTienda`;
CREATE TABLE `productoTienda` (
  `idProductoTienda` int(11) NOT NULL,
  `idProd` int(11) NOT NULL,
  `idTienda` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `precio` float NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productoTienda`
--

INSERT INTO `productoTienda` (`idProductoTienda`, `idProd`, `idTienda`, `stock`, `precio`, `estado`) VALUES
(1, 1, 1, 28, 22.08, 2),
(3, 1, 3, 82, 22, 4),
(4, 3, 1, 0, 32.5, 2),
(20, 17, 3, 99, 22.5, 1),
(21, 16, 1, 80, 25.88, 2),
(22, 18, 1, 58, 20.5, 2),
(23, 18, 3, 51, 22.2, 2),
(24, 17, 1, 34, 30, 1),
(25, 16, 3, 50, 40.25, 4),
(27, 3, 3, 90, 40.5, 1),
(28, 2, 1, 50, 20.08, 2),
(29, 2, 3, 80, 24.18, 1),
(33, 32, 1, 78, 32.29, 2),
(34, 33, 1, 80, 50.2, 1),
(35, 34, 3, 50, 89.14, 1),
(36, 35, 1, 50, 0.17, 1),
(37, 36, 1, 90, 0.49, 1),
(38, 37, 1, 80, 0.27, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservaEvent`
--

DROP TABLE IF EXISTS `reservaEvent`;
CREATE TABLE `reservaEvent` (
  `idReserva` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idEvento` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precioTotal` float NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reservaEvent`
--

INSERT INTO `reservaEvent` (`idReserva`, `idUsuario`, `idEvento`, `cantidad`, `precioTotal`, `fecha`) VALUES
(1, 3, 1, 2, 101, '2018-05-27'),
(2, 3, 1, 4, 202, '2018-05-27'),
(3, 12, 1, 4, 202, '2018-05-30'),
(4, 13, 1, 8, 404, '2018-05-30'),
(5, 10, 7, 4, 400, '2018-05-30'),
(6, 10, 10, 10, 2000, '2018-05-30'),
(7, 10, 4, 14, 700, '2018-05-30'),
(8, 10, 9, 10, 500, '2018-05-30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservaProd`
--

DROP TABLE IF EXISTS `reservaProd`;
CREATE TABLE `reservaProd` (
  `idReserva` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `idTienda` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precioTotal` float NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reservaProd`
--

INSERT INTO `reservaProd` (`idReserva`, `idUsuario`, `idProducto`, `idTienda`, `cantidad`, `precioTotal`, `fecha`) VALUES
(1, 3, 1, 1, 12, 264.96, '2018-05-27'),
(2, 3, 1, 1, 12, 264.96, '2018-05-27'),
(3, 3, 1, 1, 12, 264.96, '2018-05-27'),
(4, 3, 1, 1, 12, 264.96, '2018-05-27'),
(5, 3, 1, 1, 4, 88.32, '2018-05-27'),
(6, 3, 1, 1, 4, 88.32, '2018-05-27'),
(7, 3, 1, 1, 4, 88.32, '2018-05-27'),
(8, 3, 1, 1, 4, 88.32, '2018-05-27'),
(9, 3, 1, 1, 8, 176.64, '2018-05-27'),
(10, 3, 1, 1, 8, 176.64, '2018-05-27'),
(11, 3, 1, 1, 2, 44.16, '2018-05-27'),
(14, 3, 1, 1, 8, 176.64, '2018-05-27'),
(19, 3, 1, 1, 2, 44.16, '2018-05-27'),
(21, 3, 3, 1, 4, 130, '2018-05-27'),
(22, 3, 3, 1, 4, 130, '2018-05-27'),
(23, 12, 1, 1, 8, 176.64, '2018-05-30'),
(24, 12, 3, 1, 30, 975, '2018-05-30'),
(25, 12, 18, 1, 22, 451, '2018-05-30'),
(26, 13, 18, 3, 4, 88.8, '2018-05-30'),
(27, 13, 32, 1, 12, 387.48, '2018-05-30'),
(28, 10, 1, 1, 2, 44.16, '2018-05-30'),
(29, 10, 3, 1, 2, 65, '2018-05-30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienda`
--

DROP TABLE IF EXISTS `tienda`;
CREATE TABLE `tienda` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `direccion` varchar(45) CHARACTER SET latin1 NOT NULL,
  `ciudad` varchar(45) CHARACTER SET latin1 NOT NULL,
  `codigoPostal` int(11) NOT NULL,
  `telefono` varchar(15) CHARACTER SET latin1 NOT NULL,
  `email` varchar(45) CHARACTER SET latin1 NOT NULL,
  `fax` varchar(15) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tienda`
--

INSERT INTO `tienda` (`id`, `nombre`, `direccion`, `ciudad`, `codigoPostal`, `telefono`, `email`, `fax`) VALUES
(1, 'La Tienducha', 'calle Nose Nº4', 'Vigo', 36000, '600000000', 'emailfalso@email.com', '900000000'),
(3, 'La Esquina Shopping', 'calle Xouba Nº44', 'A Coruña', 15000, '600000000', 'emailfalsolaesquina@gmail.com', '900000000'),
(4, 'guhbu', 'nkjnmlk', 'joinj', 77777, '777777777', 'waddAN@HGVG.COM', '7777777777'),
(5, 'modifffff n', 'modifffffffffff n', 'EWRERW mod', 777777777, '88888888888', 'ASDDBASH@gggggg.com', '77777777');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoUsuario`
--

DROP TABLE IF EXISTS `tipoUsuario`;
CREATE TABLE `tipoUsuario` (
  `id` int(11) NOT NULL,
  `tipo` varchar(45) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoUsuario`
--

INSERT INTO `tipoUsuario` (`id`, `tipo`) VALUES
(1, 'Administrador'),
(2, 'Gestor'),
(3, 'Registrado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `NIF` varchar(9) DEFAULT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido1` varchar(45) NOT NULL,
  `apellido2` varchar(45) NOT NULL,
  `telefono` varchar(15) CHARACTER SET latin1 NOT NULL,
  `email` varchar(50) NOT NULL,
  `direccion` varchar(60) NOT NULL,
  `localidad` varchar(45) NOT NULL,
  `provincia` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `tipo` int(11) NOT NULL,
  `fechaSesion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `NIF`, `nombre`, `apellido1`, `apellido2`, `telefono`, `email`, `direccion`, `localidad`, `provincia`, `password`, `tipo`, `fechaSesion`) VALUES
(1, '00000000E', 'Manolo', 'Rodriguez', 'Ortiz', '700000000', 'manolo@mail.com', 'calle Nose Nº 22', 'Madrid', 'Madrid', '$2y$10$IOsxdsTPwO7kc5exFAMFzOPF3Qclm/4erSVXIzatyJFOe5I2MqQPO', 1, '2018-05-30 04:29:16'),
(3, '00000000E', 'Eugenia', 'Garrido', 'Gonzalez', '700000010', 'eugenia@mail.com', 'calle Taboada Nº 18', 'A Coruña', 'A Coruña', '$2y$10$0pqa7GPCkdpPqS.e7NtYje5YJBeXQiWPA8iujEyfZvjzOC8Apq8F2', 3, '2018-05-29 09:13:12'),
(9, '00000000F', 'Fermín', 'Perez', 'Estévez', '700000000', 'fermin@mail.com', 'calle troncoso Nª 99', 'Gijon', 'Asturias', '$2y$10$s7KvsYbBrB0cjYlVpurSg.Hi9KnDxYZOJU3mzjSYSa2gbudJJmVUy', 2, '2018-05-29 09:15:25'),
(10, '00000000P', 'Pakito', 'Ordoñez', 'Campos', '700000001', 'pakito@mail.com', 'calle Os Cantores Nº 82', 'Lugo', 'Lugo', '$2y$10$ah5AzwV26eonBf9E2Q4eiOjuON6DQIiUY3GtfO7mYssX5Fst3tYFS', 3, '2018-05-30 04:28:12'),
(12, '12345678C', 'Carlos', 'Bartolome', 'Rodriguez', '60000000', 'carlos@mail.com', 'calle Olvidada Nª 33', 'Ourense', 'Ourense', '$2y$10$F6fhSfDc8oaGYoB5MRzdJOBZtq50IJ6h2XCsHFGouhkmg5muUIoAe', 3, '2018-05-30 03:48:12'),
(13, '12345678R', 'Raúl', 'Pérez', 'Pinzón', '765432189', 'raul@mail.com', 'calle Cantores Nª 40', 'Madrid', 'Madrid', '$2y$10$Gaog3YhelvpCVE5I282f7uAiBVPdSdQ89HOi2OGGxKZEn95NwuHde', 3, '2018-05-30 03:59:01');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `eventoLugar`
--
ALTER TABLE `eventoLugar`
  ADD PRIMARY KEY (`idEventoLugar`),
  ADD KEY `idEvento` (`idEvento`),
  ADD KEY `idLugar` (`idLugar`);

--
-- Indices de la tabla `eventoParticipante`
--
ALTER TABLE `eventoParticipante`
  ADD PRIMARY KEY (`idEvento`,`idParticipante`),
  ADD KEY `idParticipante` (`idParticipante`);

--
-- Indices de la tabla `fechasPosibles`
--
ALTER TABLE `fechasPosibles`
  ADD PRIMARY KEY (`idEvento`,`fecha`),
  ADD KEY `idEvento` (`idEvento`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `CIF` (`CIF`);

--
-- Indices de la tabla `imagenesProd`
--
ALTER TABLE `imagenesProd`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ruta` (`ruta`),
  ADD KEY `idProd` (`idProd`);

--
-- Indices de la tabla `lugar`
--
ALTER TABLE `lugar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lugar` (`lugar`);

--
-- Indices de la tabla `participante`
--
ALTER TABLE `participante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grupo` (`grupo`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoria` (`categoria`);

--
-- Indices de la tabla `productoTienda`
--
ALTER TABLE `productoTienda`
  ADD PRIMARY KEY (`idProductoTienda`),
  ADD UNIQUE KEY `idProd_2` (`idProd`,`idTienda`,`estado`),
  ADD KEY `idProd` (`idProd`),
  ADD KEY `idTienda` (`idTienda`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `reservaEvent`
--
ALTER TABLE `reservaEvent`
  ADD PRIMARY KEY (`idReserva`),
  ADD KEY `idUsuario` (`idUsuario`),
  ADD KEY `idEvento` (`idEvento`),
  ADD KEY `idUsuario_2` (`idUsuario`,`idEvento`);

--
-- Indices de la tabla `reservaProd`
--
ALTER TABLE `reservaProd`
  ADD PRIMARY KEY (`idReserva`),
  ADD KEY `idUsuario` (`idUsuario`),
  ADD KEY `idProducto` (`idProducto`),
  ADD KEY `idTienda` (`idTienda`),
  ADD KEY `idUsuario_2` (`idUsuario`,`idProducto`,`idTienda`);

--
-- Indices de la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipoUsuario`
--
ALTER TABLE `tipoUsuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `tipo` (`tipo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `evento`
--
ALTER TABLE `evento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `eventoLugar`
--
ALTER TABLE `eventoLugar`
  MODIFY `idEventoLugar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `imagenesProd`
--
ALTER TABLE `imagenesProd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `lugar`
--
ALTER TABLE `lugar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `participante`
--
ALTER TABLE `participante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `productoTienda`
--
ALTER TABLE `productoTienda`
  MODIFY `idProductoTienda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `reservaEvent`
--
ALTER TABLE `reservaEvent`
  MODIFY `idReserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `reservaProd`
--
ALTER TABLE `reservaProd`
  MODIFY `idReserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `tienda`
--
ALTER TABLE `tienda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tipoUsuario`
--
ALTER TABLE `tipoUsuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `eventoLugar`
--
ALTER TABLE `eventoLugar`
  ADD CONSTRAINT `eventoLugar_ibfk_1` FOREIGN KEY (`idEvento`) REFERENCES `evento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `eventoLugar_ibfk_2` FOREIGN KEY (`idLugar`) REFERENCES `lugar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `eventoParticipante`
--
ALTER TABLE `eventoParticipante`
  ADD CONSTRAINT `eventoParticipante_ibfk_1` FOREIGN KEY (`idEvento`) REFERENCES `eventoLugar` (`idEventoLugar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `eventoParticipante_ibfk_2` FOREIGN KEY (`idParticipante`) REFERENCES `participante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `fechasPosibles`
--
ALTER TABLE `fechasPosibles`
  ADD CONSTRAINT `fk_evento` FOREIGN KEY (`idEvento`) REFERENCES `eventoLugar` (`idEventoLugar`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `imagenesProd`
--
ALTER TABLE `imagenesProd`
  ADD CONSTRAINT `imagenesProd_ibfk_1` FOREIGN KEY (`idProd`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `participante`
--
ALTER TABLE `participante`
  ADD CONSTRAINT `participante_ibfk_1` FOREIGN KEY (`grupo`) REFERENCES `grupo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`categoria`) REFERENCES `categoria` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `productoTienda`
--
ALTER TABLE `productoTienda`
  ADD CONSTRAINT `productoTienda_ibfk_1` FOREIGN KEY (`idProd`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productoTienda_ibfk_2` FOREIGN KEY (`idTienda`) REFERENCES `tienda` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productoTienda_ibfk_3` FOREIGN KEY (`estado`) REFERENCES `estado` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `reservaEvent`
--
ALTER TABLE `reservaEvent`
  ADD CONSTRAINT `reservaEvent_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservaEvent_ibfk_2` FOREIGN KEY (`idEvento`) REFERENCES `eventoLugar` (`idEventoLugar`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `reservaProd`
--
ALTER TABLE `reservaProd`
  ADD CONSTRAINT `reservaProd_ibfk_1` FOREIGN KEY (`idProducto`) REFERENCES `productoTienda` (`idProd`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservaProd_ibfk_3` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservaProd_ibfk_4` FOREIGN KEY (`idTienda`) REFERENCES `productoTienda` (`idTienda`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `tipoUsuario` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
