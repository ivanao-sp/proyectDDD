
var miApp = angular.module("proyectApp",["admiPrApp","admiEvApp","gestPrApp","gestEvApp","sesionApp","registroApp","tiendasApp","modifPerfilApp","reserPrApp","reserEvApp","verPrApp","verEvApp","listasApp","ngRoute"])

.factory('myuser', function (){
		
		var plantilla = {
							id       : "",
							nif      : "",
							nom      : "Anónimo",
							ape1     : "",
							ape2     : "",
							email    : "",
							tel      : "",
							dir      : "",
							local    : "",
							prov     : "",
							permisos : 4,
							fecha    : "",
							logeado  : "no"
		};
		var servicio = {};
		var myuser = plantilla;

		var getID = function(){
			return myuser.id;			
		}

		var getUser = function(){
			return myuser;
		}

		var setUser = function(user){
			myuser = user;
		}

		var getNom = function(){
			return myuser.nom;
		}

		var getNomCom = function(){
			return (myuser.nom + " " + myuser.ape1 + " " + myuser.ape2);
		}

		var getEmail = function(){
			return myuser.email;
		}

		var getTipo = function(){
			return myuser.permisos;
		}

		var getFecha = function(){
			return myuser.fecha;
		}

		var getNif = function(){
			return myuser.nif;
		}

		var getApe1 = function(){
			return myuser.ape1;
		}

		var getApe2 = function(){
			return myuser.ape2;
		}

		var getTel = function(){
			return myuser.tel;
		}

		var getDir = function(){
			return myuser.dir;
		}

		var getLocal = function(){
			return myuser.local;
		}

		var getProv = function(){
			return myuser.prov;
		}

		var clearUsuario = function(){
			myuser = plantilla;
			return myuser;
		}

		var staLogeado = function(){
			return myuser.logeado;
		}
		
		servicio.getID = getID;
		servicio.getUser = getUser;
		servicio.setUser = setUser;
		servicio.getNom = getNom;
		servicio.getNomCom = getNomCom;
		servicio.getEmail = getEmail;
		servicio.getTipo = getTipo;
		servicio.getFecha = getFecha;
		servicio.getNif = getNif;
		servicio.getApe1 = getApe1;
		servicio.getApe2 = getApe2;
		servicio.getTel = getTel;
		servicio.getDir = getDir;
		servicio.getLocal = getLocal;
		servicio.getProv = getProv;
		servicio.clearUsuario = clearUsuario;
		servicio.staLogeado = staLogeado;		
		return servicio;
	});


miApp.controller('mainCtrl', function($scope){

})

miApp.controller('navCtrl', function($scope,$http,$location,myuser){
	$(function() {
   		$(".button-collapse").sideNav();
   		$(".dropdown-button").dropdown();   
	});

	$scope.muestraMens = function(elMensaje) {
		$scope.mensaje = elMensaje;
		$('#mymodal2').modal('open');
	}

	$scope.copPlantilla = {
							id : "",
							nif : "",
							nom : "Anónimo",
							ape1 : "",
							ape2 : "",
							email : "",
							tel : "",
							dir : "",
							local : "",
							prov : "",
							permisos : 4,
							fecha : "",
							logeado : "no"
		};

	$scope.elUsuario = myuser;

	$scope.cerrarSesion = function() {

		var peticion = {
		"url"    : "php/funcionalidades/sesion.php",
		"method" : "POST",
		"data"   : { opcion     : 'cerrar',
					 parametros : 'nada'
				   }
		};

		$http(peticion).then(function(bien){
		$scope.respuesta = angular.copy(bien.data);
			if($scope.respuesta.estado == undefined) {
				$scope.muestraMens("Error desconocido");	
			} else {
				 if($scope.respuesta.estado == "ok") {

					 myuser.setUser($scope.copPlantilla);
					 
					 $location.path('/inicio');

				  } else {
				  	  $scope.muestraMens($scope.respuesta.tipoError);
			 	   }
			 }
		},

		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

})

miApp.controller('pieCtrl', function($scope,myuser){
	$scope.elUsuario = myuser;
})

.constant('vistaext','view.html')
.config(function($routeProvider,$locationProvider,vistaext){
	// $routeProvider -> gestiona las rutas
	// $locationProvider -> gestiona la barra de navegación
	$locationProvider.hashPrefix('');
	$routeProvider
		.when('/inicio', {
			templateUrl : 'js/vistas/home.'+vistaext
		})

		.when('/verPrductos', {
			templateUrl : 'js/vistas/verPro.'+vistaext,
			controller 	: 'produVerCtrl'
		})

		.when('/reserProductos', {
			templateUrl : 'js/vistas/reserProd.'+vistaext,
			controller 	: 'produReserCtrl'
		})

		.when('/reserEventos', {
			templateUrl : 'js/vistas/reserEvent.'+vistaext,
			controller 	: 'eventReserCtrl'
		})

		.when('/gestProductos', {
			templateUrl : 'js/vistas/geProd.'+vistaext,
			controller 	: 'produGestCtrl'
		})

		.when('/admiProductos', {
			templateUrl : 'js/vistas/adProd.'+vistaext,
			controller 	: 'produAdmiCtrl'
		})

		.when('/verEventos', {
			templateUrl : 'js/vistas/verEve.'+vistaext,
			controller 	: 'eventVerCtrl'
		})

		.when('/gestEventos', {
			templateUrl : 'js/vistas/geEve.'+vistaext,
			controller 	: 'eventGestCtrl'
		})

		.when('/admiTiendas', {
			templateUrl : 'js/vistas/tiendas.'+vistaext,
			controller 	: 'tiendsCtrl'
		})

		.when('/admiEventos', {
			templateUrl : 'js/vistas/adEve.'+vistaext,
			controller 	: 'eventAdmiCtrl'
		})

		.when('/registro', {
			templateUrl : 'js/vistas/regist.'+vistaext,
			controller 	: 'registCtrl'
		})

		.when('/iniSesion', {
			templateUrl : 'js/vistas/iniSesion.'+vistaext,
			controller 	: 'sesionCtrl'
		})

		.when('/modPerfil', {
			templateUrl : 'js/vistas/modiPerfil.'+vistaext,
			controller 	: 'modiPerfilCtrl'
		})

		.when('/listas', {
			templateUrl : 'js/vistas/listas.'+vistaext,
			controller 	: 'listasCtrl'
		})

		.when('/docu', {
			templateUrl : 'js/vistas/docu.'+vistaext,
		})

		.otherwise({
        	redirectTo:'/inicio'
        });

	
})

.component('cabecera', {
	templateUrl: 'js/vistas/cabecera.view.html',
	controller: 'navCtrl'
})

.component('pie', {
	templateUrl: 'js/vistas/pie.view.html',
	controller: 'pieCtrl'
});

