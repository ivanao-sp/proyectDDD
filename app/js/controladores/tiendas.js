var tiendsApp = angular.module("tiendasApp",[]);

tiendsApp.controller('tiendsCtrl', function($scope,$http){

	$('.modal').modal();

	$('select').material_select();

	$scope.muestraMens = function(elMensaje) {
		$scope.mensaje = elMensaje;
		$('#mymodal2').modal('open');
	}

	$scope.cerrarModal = function(referencia) {
		$(referencia).modal('close');
	}

	$scope.datoss = function() {

		var peticion = {
		"url"    : "php/funcionalidades/mostrarAdGe.php",
		"method" : "POST",
		"data"   : { opcion : "tiendas" }
		};

		$http(peticion).then(function(bien){

			if(bien.data.estado == "ok") {

				$scope.tiendas = angular.copy(bien.data.tiendas);

			} else {

				 $scope.muestraMens(bien.data.tipoError);

			 }
		},
		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.datoss();

	$scope.anhadir = function() {

		$scope.datos = {
					'id'            : null,
					'nombre'        : null,
					'direccion'     : null,
					'ciudad'        : null,
					'codigoPostal'  : null,
					'telefono'      : null,
					'email'         : null,
					'fax'           : null,
				};

		$scope.modal = {
			'titulo'   : ' Añadir Nueva Tienda',
			'icono'    : 'note_add',
			'hColor'   : 'green lighten-4',
			'rCampos'  : '',
			'boton'    : 'Añadir',
			'bIcono'   : 'save',
			'ocultar'  : '',
			'btTipo'   : 'boton-verde',
			'datos'    : $scope.datos

 		};
	}

	$scope.modif = function() {
		$scope.miThis = this;
		$scope.copNomTien = angular.copy($scope.nombreTiendas);
		$scope.datos=[];
		(['id','nombre','direccion','ciudad','codigoPostal','telefono','email','fax']).forEach(function(valor,indice){
			$scope.datos[valor]=$scope.miThis.tienda[valor];	 
		});

		$scope.modal = {
			'titulo'   : ' Editar Producto '+$scope.datos.nombre,
			'icono'    : 'border_color',
			'hColor'   : 'indigo lighten-4',
			'rCampos'  : 'resalte-campos',
			'qeHacer'  : '',
			'boton'    : 'Guardar',
			'bIcono'   : 'save',
			'btTipo'   : 'boton-azul',
			'ocultar'  : 'display:none;',
			'datos'    : $scope.datos,

 		};
	}

	$scope.anhadirArriba = function() {
		$scope.datosT = angular.copy($scope.modal.datos);

		var peticion = {
		"url"    : "php/funcionalidades/administrar.php",
		"method" : "POST",
		"data"   : { opcion     : "tienda",
					 accion     : "anhadir",
					 parametros : $scope.datosT 
				   }
		};

		$http(peticion).then(function(bien){
		$scope.respuesta = angular.copy(bien.data);
			if($scope.respuesta.estado == undefined) {
				$scope.muestraMens("Error desconocido");
			} else {
				 if($scope.respuesta.estado == "ok") {				
					 $scope.datoss();
					 $scope.cerrarModal('#mymodal1');
					 $scope.muestraMens($scope.respuesta.mens);	
				  } else {
				  	  $scope.cerrarModal('#mymodal1');
				  	  $scope.muestraMens($scope.respuesta.tipoError);
			 	   }
			 }
		},

		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.modificarArriba = function() {

		var peticion = {
		"url"    : "php/funcionalidades/administrar.php",
		"method" : "POST",
		"data"   : { opcion     : "tienda",
					 accion     : "modif",
					 parametros : { id           : parseInt($scope.modal.datos.id),
					 				nombre       : $scope.modal.datos.nombre,
					 				direccion    : $scope.modal.datos.direccion,
					 				ciudad       : $scope.modal.datos.ciudad,
					 				codigoPostal : $scope.modal.datos.codigoPostal,
					 				telefono     : $scope.modal.datos.telefono,
					 				email        : $scope.modal.datos.email,
					 				fax          : $scope.modal.datos.fax
					 			  }
				   }
		};

		$http(peticion).then(function(bien){
		$scope.respuesta = angular.copy(bien.data);
			if($scope.respuesta.estado == undefined) {
				$scope.muestraMens("Error desconocido");
			} else {
				 if($scope.respuesta.estado == "ok") {				
					 $scope.datoss();
					 $scope.cerrarModal('#mymodal1');
					 $scope.muestraMens($scope.respuesta.mens);	
				  } else {
				  	  $scope.cerrarModal('#mymodal1');
				  	  $scope.muestraMens($scope.respuesta.tipoError);
			 	   }
			 }
		},

		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.queHacer = function() {
		switch ($scope.modal.boton) {
			case "Añadir":
				$scope.anhadirArriba();
			break;
			case "Guardar":
				$scope.modificarArriba();
			break;
			default:
				alert("Error");
			break;
		}
	}

}).

component('lasTiendas', {
	templateUrl: 'js/vistas/tiendas.view.html',
	controller: 'tiendsCtrl'
});