var listadosApp = angular.module("listasApp",[]);

listadosApp.controller('listasCtrl', function($scope,$http){

	$('.modal').modal();

	$('select').material_select();

	$('.carousel').carousel();

	$('.slider').slider();

	$scope.muestraMens = function(elMensaje) {
		$scope.mensaje = elMensaje;
		$('#mymodal2').modal('open');
	}

	$scope.cerrarModal = function(referencia) {
		$(referencia).modal('close');
	}

	$scope.ceros = function(nro) {
		return ('0'+nro).slice(-2);
	}

		$scope.buscar= function() {

		$scope.fecha1 = $scope.rango1.getFullYear()+"-"+$scope.ceros($scope.rango1.getMonth())+"-"+$scope.ceros($scope.rango1.getDate());
		$scope.fecha2 = $scope.rango2.getFullYear()+"-"+$scope.ceros($scope.rango2.getMonth())+"-"+$scope.ceros($scope.rango2.getDate());	
	
		var peticion = {
		"url"    : "php/funcionalidades/mostrarAdGe.php",
		"method" : "POST",
		"data"   : { opcion      : "reservas",
					 limitacion1 : $scope.fecha1,
					 limitacion2 : $scope.fecha2 }
		};

		$http(peticion).then(function(bien){

			if(bien.data.estado == "ok") {

				$scope.listaP = angular.copy(bien.data.reserPro);
				$scope.listaE = angular.copy(bien.data.reserEve);

			} else {

				 $scope.muestraMens(bien.data.tipoError);

			 }
		},
		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}


}).

component('listados', {
	templateUrl: 'js/vistas/listas.view.html',
	controller: 'listasCtrl'
});