var sesiionApp = angular.module("sesionApp",[]);

sesiionApp.controller('sesionCtrl', function($scope,$http,myuser,$location){
	
	$('.modal').modal();

	$('select').material_select();

	$scope.muestraMens = function(elMensaje) {
		$scope.mensaje = elMensaje;
		$('#mymodalInfo').modal('open');
	}

	$scope.cerrarModal = function(referencia) {
		$(referencia).modal('close');
	}

	$scope.arribaa = function() {
		
		var peticion = {
		"url"    : "php/funcionalidades/sesion.php",
		"method" : "POST",
		"data"   : { opcion     : 'iniciar',
					 parametros : { 'e' : $scope.elEmail, 'p' : $scope.laContra }
				   }
		};

		$http(peticion).then(function(bien){
		$scope.respuesta = angular.copy(bien.data);
			if($scope.respuesta.estado == undefined) {
				$scope.muestraMens("Error desconocido");	
			} else {
				 if($scope.respuesta.estado == "ok") {

				 	 $scope.usuarioRecogido = $scope.respuesta.sesionn;

					 myuser.setUser($scope.usuarioRecogido);
					 
					 $location.path('/inicio');

				  } else {
				  	  $scope.muestraMens($scope.respuesta.tipoError);
			 	   }
			 }
		},

		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

}).

component('iniciarSesion', {
	templateUrl: 'js/vistas/iniSesion.view.html',
	controller: 'sesionCtrl'
});