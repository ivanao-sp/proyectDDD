var modiPerfilApp = angular.module("modifPerfilApp",[]);

modiPerfilApp.controller('modiPerfilCtrl', function($scope,$http,myuser,$location){

	$('.modal').modal();

	$('select').material_select();

	$scope.auxM = false;

	$scope.muestraMens = function(elMensaje) {
		$scope.mensaje = elMensaje;
		$('#mymodalM').modal('open');
	}

	$scope.cerrarModal = function(referencia) {
		$(referencia).modal('close');

		if($scope.auxM){
			$scope.auxM = false;
			$location.path('/inicio');	 
		}
	}

	$scope.ultiIniSesion = myuser.getFecha().split(" ")[0].split("-")[2]+" del "+myuser.getFecha().split(" ")[0].split("-")[1]+" de "+myuser.getFecha().split(" ")[0].split("-")[0]+" a las "+myuser.getFecha().split(" ")[1];
	$scope.elEmailMod = myuser.getEmail();
	$scope.nifMod = myuser.getNif();
	$scope.elnombreMod = myuser.getNom();
	$scope.ape1Mod = myuser.getApe1();
	$scope.ape2Mod = myuser.getApe2();
	$scope.telMod = myuser.getTel();
	$scope.direMod = myuser.getDir();
	$scope.localMod = myuser.getLocal();
	$scope.provMod = myuser.getProv();

	$scope.modificArriba = function() {

		if($scope.laContraMod != $scope.laContra2Mod) {
			$scope.muestraMens("las contraseñas no coinciden");
		} else {
			 if($scope.laContraMod == undefined){
			 	$scope.laContraMod = "";
			 }	

			 var peticion = {
			 "url"    : "php/funcionalidades/sesion.php",
			 "method" : "POST",
			 "data"   : { opcion     : "modificar",
					 	  parametros : { 'id'  : myuser.getID(),
					 	  			     'e'   : $scope.elEmailMod,
					 	  				 'n'   : $scope.nifMod,
					 	  				 'nom' : $scope.elnombreMod,
					 	  				 'a1'  : $scope.ape1Mod,
					 	  				 'a2'  : $scope.ape2Mod,
					 	  				 't'   : $scope.telMod,
					 	  				 'd'   : $scope.direMod,
					 	  				 'l'   : $scope.localMod, 
					 	  				 'p'   : $scope.provMod, 
					 			       	 'c'   : $scope.laContraMod }
				   						}
				   		}
			 };

			 $http(peticion).then(function(bien){
			 $scope.respuesta = angular.copy(bien.data);
				 if($scope.respuesta.estado == undefined) {
					 $scope.muestraMens("Error desconocido");
				 } else {
				 	 if($scope.respuesta.estado == "ok") {

				 	 	 $scope.cambio = {       id : myuser.getID(),
				 	 	 					    nif : $scope.nifMod,
				 	 	 				        nom : $scope.elnombreMod,
				 	 	 					   ape1 : $scope.ape1Mod,
					 	  				       ape2 : $scope.ape2Mod,
					 	  				      email : $scope.elEmailMod,
					 	  				        tel : $scope.telMod,
					 	  				        dir : $scope.direMod,
					 	  				      local : $scope.localMod,
					 	  				       prov : $scope.provMod,
					 	  				   permisos : myuser.getTipo(),
				 	 	 				      fecha : myuser.getFecha(), 
					 			       	    logeado : "si" };

					 	 $scope.usuarioRecogido = $scope.cambio;

					 	 myuser.setUser($scope.usuarioRecogido);

					 	 $scope.auxM = true;		       	    	

					 	 $scope.muestraMens($scope.respuesta.mens);	
				  	 } else {
				  	  	 $scope.muestraMens($scope.respuesta.tipoError);
			 	   	 }
			 	 }
			 },

		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}
	

}).

component('modificarPerfil', {
	templateUrl: 'js/vistas/modiPerfil.view.html',
	controller: 'modiPerfilCtrl'
});
