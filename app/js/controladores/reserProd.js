var resProApp = angular.module("reserPrApp",[]);

resProApp.controller('produReserCtrl', function($scope,$http){

	$('.modal').modal();

	$('select').material_select();

	$('.carousel').carousel();

	$('.slider').slider();

	$scope.muestraMens = function(elMensaje) {
		$scope.mensaje = elMensaje;
		$('#mymodal2').modal('open');
	}

	$scope.cerrarModal = function(referencia) {
		$(referencia).modal('close');
	}

	$scope.datoss = function() {

		var peticion = {
		"url"    : "php/funcionalidades/mostrarReAn.php",
		"method" : "POST",
		"data"   : { opcion : "productos" }
		};

		$http(peticion).then(function(bien){

			if(bien.data.estado == "ok") {

				$scope.articulos = angular.copy(bien.data.arrayProds);
				$scope.categorias = angular.copy(bien.data.categorias);
				$scope.tiendas = angular.copy(bien.data.arrayTiendas);

				$scope.nombreTiendas = [];

				$scope.tiendas.forEach(function(valor,indice){
					valor["id"] = parseInt(valor["id"]);
					$scope.aux = angular.copy(valor["nombre"]+" - "+valor["ciudad"]);
					$scope.nombreTiendas[valor["id"].toString()] = angular.copy($scope.aux);	
				});

			} else {

				 $scope.muestraMens(bien.data.tipoError);

			 }
		},
		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.datoss();

	$scope.reservar = function() {

		$scope.miThis = this;
		$scope.idProd = $scope.miThis.articulo.id;
		$scope.cantidad = $scope.miThis.cantidad;

		var peticion = {
		"url"    : "php/funcionalidades/reservar.php",
		"method" : "POST",
		"data"   : { opcion     : "producto",
					 parametros : { 'prod' : $scope.idProd,
					 				'cant' : $scope.cantidad
					              }
				   }
		};

		$http(peticion).then(function(bien){
		$scope.respuesta = angular.copy(bien.data);
			if($scope.respuesta.estado == undefined) {
				$scope.muestraMens("Error desconocido");
			} else {
				 if($scope.respuesta.estado == "ok") {				
					 $scope.datoss();
					 $scope.muestraMens($scope.respuesta.mens);	
				  } else {
				  	  $scope.muestraMens($scope.respuesta.tipoError);
			 	   }
			 }
		},

		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

}).

component('reservarArts', {
	templateUrl: 'js/vistas/reserProd.view.html',
	controller: 'produReserCtrl'
});