var veerProApp = angular.module("verPrApp",[]);

veerProApp.controller('produVerCtrl', function($scope,$http){

	$('.modal').modal();

	$('select').material_select();

	$scope.muestraMens = function(elMensaje) {
		$scope.mensaje = elMensaje;
		$('#mymodal2').modal('open');
	}

	$scope.cerrarModal = function(referencia) {
		$(referencia).modal('close');
	}

	$scope.cambio = function(miIndice) {

		if ($scope.articulos[miIndice]["posFoto"] == $scope.articulos[miIndice]["posCant"]) {

			$scope.articulos[miIndice]["posFoto"] = 0;

		} else {

			$scope.articulos[miIndice]["posFoto"] = $scope.articulos[miIndice]["posFoto"]+1;

		}

		$scope.articulos[miIndice].arrayIma.forEach(function(valor,indice) {
			if (indice == $scope.articulos[miIndice].posFoto) {
				valor.mostrar = "block";
			} else {
				valor.mostrar = "none";
			}
		});

	}

	$scope.datoss = function() {

		var peticion = {
		"url"    : "php/funcionalidades/mostrarReAn.php",
		"method" : "POST",
		"data"   : { opcion : "productos" }
		};

		$http(peticion).then(function(bien) {

			if(bien.data.estado == "ok") {

				$scope.articulos = angular.copy(bien.data.arrayProds);
				$scope.categorias = angular.copy(bien.data.categorias);
				$scope.tiendas = angular.copy(bien.data.arrayTiendas);

				$scope.nombreTiendas = [];

				$scope.tiendas.forEach(function(valor,indice) {
					valor["id"] = parseInt(valor["id"]);
					$scope.aux = angular.copy(valor["nombre"]+" - "+valor["ciudad"]);
					$scope.nombreTiendas[valor["id"].toString()] = angular.copy($scope.aux);	
				});

				$scope.articulos.forEach(function(valor,indice) {
					valor["myIndex"] = indice;
					valor["posFoto"] = 0;
					valor["posCant"] = valor["arrayIma"].length-1;

					valor.arrayIma.forEach(function(valor,indice) {
						if (indice == 0) {
							valor["mostrar"] = "block";
						} else {
							valor["mostrar"] = "none";
						}
					});

				});

			} else {

				 $scope.muestraMens(bien.data.tipoError);

			 }
		},
		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.datoss();

}).

component('verLosArts', {
	templateUrl: 'js/vistas/verProd.view.html',
	controller: 'produVerCtrl'
});