var registApp = angular.module("registroApp",[]);

registApp.controller('registCtrl', function($scope,$http,$location){

	$('.modal').modal();

	$('select').material_select();

	$scope.aux = false;  

	$scope.muestraMens = function(elMensaje) {
		$scope.mensaje = elMensaje;
		$('#mymodalR').modal('open');
	}

	$scope.cerrarModal = function(referencia) {
		$(referencia).modal('close');

		if($scope.aux){
			$scope.aux = false;
			$location.path('/iniSesion');	 
		}
	}

	$scope.regArriba = function() {

		if($scope.laContra != $scope.laContra2){

			muestraMens("Las contraseñas no coinviden");

		} else {
			 var peticion = {
			 "url"    : "php/funcionalidades/sesion.php",
			 "method" : "POST",
			 "data"   : { opcion     : 'registrar',
					 	  parametros : { 'e'   : $scope.elEmail,
					 	  				 'n'   : $scope.nif,
					 	  				 'nom' : $scope.elnombre,
					 	  				 'a1'  : $scope.ape1,
					 	  				 'a2'  : $scope.ape2,
					 	  				 't'   : $scope.tel,
					 	  				 'd'   : $scope.dire,
					 	  				 'l'   : $scope.local, 
					 	  				 'p'   : $scope.prov, 
					 			       	 'c'   : $scope.laContra }
				   		}
			 };

			 $http(peticion).then(function(bien){

				 $scope.respuesta = angular.copy(bien.data);

				 if($scope.respuesta.estado == undefined) {
				     $scope.muestraMens("Error desconocido");
			     } else {
				 	  if($scope.respuesta.estado == "ok") {	
				 	      $scope.aux = true;		
					      $scope.muestraMens($scope.respuesta.mens+". Ahora ya puedes iniciar sesión");	
				   } else {
				  	     $scope.muestraMens($scope.respuesta.tipoError);
			 	     }
			      }
			 },
			 function(mal){
				 $scope.muestraMens("Error en conexión");
			 });
		 }
	}

}).

component('registroUsers', {
	templateUrl: 'js/vistas/regist.view.html',
	controller: 'registCtrl'
});