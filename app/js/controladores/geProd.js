
var gesProApp = angular.module("gestPrApp",[]);

gesProApp.controller('produGestCtrl', function($scope,$http){

	$('.modal').modal();

	$('select').material_select();

	$scope.ceros = function(nro) {
		return ('0'+nro).slice(-2);
	}

	$scope.staDisabled = false;

	$scope.muestraMens = function(elMensaje) {
		$scope.mensaje = elMensaje;
		$('#mymodal2').modal('open');
	}

	$scope.datoss = function() {

		var peticion = {
		"url"    : "php/funcionalidades/mostrarAdGe.php",
		"method" : "POST",
		"data"   : { opcion : "productos" }
		};

		$http(peticion).then(function(bien){

			if(bien.data.estado == "ok") {

				$scope.articulos = angular.copy(bien.data.arrayProds);
				$scope.tiendas = angular.copy(bien.data.arrayTiendas);
				$scope.infoPro = angular.copy(bien.data.infoPro);
				$scope.losEstados = angular.copy(bien.data.losEstados);
				$scope.categorias = angular.copy(bien.data.categorias);

				$scope.articulos.forEach(function(valor,indice){
					valor['fechaFinCampanha'] = new Date(valor['fechaFinCampanha']);
					valor['stock'] = parseInt(valor['stock']);
					valor['precio'] = parseFloat(valor['precio']);
				});

				$scope.nombreTiendas = [];
				$scope.nombreTiendas2 = [];
				$scope.idsTiendas = [];
				$scope.idsProds = [];
				$scope.aux = "";

				$scope.tiendas.forEach(function(valor,indice){
					valor["id"] = parseInt(valor["id"]);
					$scope.aux = angular.copy(valor["nombre"]+" - "+valor["ciudad"]);
					$scope.nombreTiendas[valor["id"].toString()] = angular.copy($scope.aux);	
				});

				$scope.infoPro.forEach(function(valor,indice){
					valor["id"] = parseInt(valor["id"]);
					$scope.idsProds[valor["nombre"].toString()] = valor["id"];		
				});

			} else {

				 $scope.muestraMens(bien.data.tipoError);

			 }
		},
		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.datoss();
	
	$scope.modif = function() {
		$scope.miThis = this;
		$scope.copNomTien = angular.copy($scope.nombreTiendas);
		$scope.datos=[];
		(['id','nombre','descripcion','categoria','fechaFinCampanha','idTienda','stock','precio','estado','arrayIma']).forEach(function(valor,indice){
			if (valor == "fechaFinCampanha") {
				$scope.datos["fecha"]=$scope.miThis.articulo[valor];
			} else {
				 $scope.datos[valor]=$scope.miThis.articulo[valor];
			 }
		});

		$scope.modal = {
			'titulo'   : ' Editar Producto '+$scope.datos.nombre,
			'icono'    : 'border_color',
			'hColor'   : 'indigo lighten-4',
			'rCampos'  : 'resalte-campos',
			'qeHacer'  : '',
			'boton'    : 'Guardar',
			'bIcono'   : 'save',
			'btTipo'   : 'boton-azul',
			'ocultar'  : 'display:none;',
			'datos'    : $scope.datos,
			'tiendas'  : $scope.copNomTien

 		};
 		$scope.ocultarA = "display:none;";
		$scope.ocultarB = "";
		$scope.ocultarC = "";
 		$scope.ocultarD = "";
	}

	$scope.anhadir = function() {

		$scope.datos = {
					'id'               : null,
					'nombre'           : null,
					'descripcion'      : null,
					'categoria'        : null,
					'fecha' 		   : null,
					'arrayIma'         : null,
					'idTienda'         : null,
					'stock'            : null,
					'precio'           : null,
					'estado'           : "Propuesto"
				};

		$scope.modal = {
			'titulo'   : ' Añadir Nuevo Producto',
			'icono'    : 'note_add',
			'hColor'   : 'green lighten-4',
			'rCampos'  : '',
			'boton'    : 'Añadir',
			'bIcono'   : 'save',
			'ocultar'  : '',
			'btTipo'   : 'boton-verde',
			'datos'    : $scope.datos

 		};

 		$scope.ocultarPro = "";
 		$scope.productoElegido = "";
 		$scope.opcionAdd = "a";
 		$scope.proCompl();
 		$scope.ocultarD = "display:none;";
	}

	$scope.proCompl = function() {
		switch ($scope.opcionAdd) {
			case "a":
			$scope.ocultarA = "display:none;";
			$scope.ocultarB = "";
			$scope.ocultarC = "";
			break;
			case "b":
			$scope.ocultarA = "display:none;";
			$scope.ocultarB = "";
			$scope.ocultarC = "display:none;";
			break;
			case "c":
			$scope.ocultarA = "";
			$scope.ocultarB = "display:none;";
			$scope.ocultarC = "";
			break;
			default:
				alert("Error");
			break;
		}
	}

	$scope.queHacer = function() {
		switch ($scope.modal.boton) {
			case "Añadir":
				$scope.anhadirArriba();
			break;
			case "Guardar":
				$scope.modificarArriba();
			break;
			case "Retirar":
				$scope.borrarArriba();
			break;
			default:
				alert("Error");
			break;
		}
	}

	$scope.anhadirArriba = function() {
		$scope.datosP = angular.copy($scope.modal.datos);
		auxF = angular.copy($scope.modal.datos.fecha);
		if($scope.productoElegido!=""){
			$scope.datosP["id"] = $scope.idsProds[$scope.productoElegido];
		} 
		
		if(auxF != null) {
			fecha = auxF.getFullYear()+"-"+$scope.ceros(auxF.getMonth()+1)+"-"+$scope.ceros(auxF.getDate());
			$scope.datosP.fecha = fecha;
		}

		var peticion = {
		"url"    : "php/funcionalidades/gestionar.php",
		"method" : "POST",
		"data"   : { opcion     : "producto",
					 accion     : "anhadir",
					 parametros : $scope.datosP 
				   }
		};

		$http(peticion).then(function(bien){
		$scope.respuesta = angular.copy(bien.data);
			if($scope.respuesta.estado == undefined) {
				$scope.muestraMens("Error desconocido");
			} else {
				 if($scope.respuesta.estado == "ok") {				
					 $scope.datoss();
					 $scope.cerrarModal('#mymodal1');
					 $scope.muestraMens($scope.respuesta.mens);	
				  } else {
				  	  $scope.cerrarModal('#mymodal1');
				  	  $scope.muestraMens($scope.respuesta.tipoError);
			 	   }
			 }
		},

		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.modificarArriba = function() {

		auxF = angular.copy($scope.modal.datos.fecha);
		
		fechaP = auxF.getFullYear()+"-"+$scope.ceros(auxF.getMonth()+1)+"-"+$scope.ceros(auxF.getDate());

		var peticion = {
		"url"    : "php/funcionalidades/gestionar.php",
		"method" : "POST",
		"data"   : { opcion     : "producto",
					 accion     : "modif",
					 parametros : { id : parseInt($scope.modal.datos.id),
					 				nombre : $scope.modal.datos.nombre,
					 				descripcion : $scope.modal.datos.descripcion,
					 				categoria : $scope.modal.datos.categoria,
					 				fecha : fechaP,
					 				idTienda : null,
					 				arrayIma : $scope.modal.datos.arrayIma,
					 				stock : $scope.modal.datos.stock,
					 				precio : $scope.modal.datos.precio,
					 				estado : $scope.modal.datos.estado
					 			  }
				   }
		};

		$http(peticion).then(function(bien){
		$scope.respuesta = angular.copy(bien.data);
			if($scope.respuesta.estado == undefined) {
				$scope.muestraMens("Error desconocido");
			} else {
				 if($scope.respuesta.estado == "ok") {				
					 $scope.datoss();
					 $scope.cerrarModal('#mymodal1');
					 $scope.muestraMens($scope.respuesta.mens);	
				  } else {
				  	  $scope.cerrarModal('#mymodal1');
				  	  $scope.muestraMens($scope.respuesta.tipoError);
			 	   }
			  }
		},

		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.cerrarModal = function(referencia) {
		$(referencia).modal('close');
	}

}).

component('gestionarArts', {
	templateUrl: 'js/vistas/geProd.view.html',
	controller: 'produGestCtrl'
});
