var gesEveApp = angular.module("gestEvApp",[]);

gesProApp.controller('eventGestCtrl', function($scope,$http){
	$('.modal').modal();

	$('select').material_select();

	$scope.ceros = function(nro) {
		return ('0'+nro).slice(-2);
	}

	$scope.staDisabled = false;

	$scope.auxiliar = false;

	$scope.muestraMens = function(elMensaje) {
		$scope.mensaje = elMensaje;
		$('#mymodal2').modal('open');
	}

	$scope.datoss = function() {

		var peticion = {
		"url"    : "php/funcionalidades/mostrarAdGe.php",
		"method" : "POST",
		"data"   : { opcion : "eventos" }
		};

		$http(peticion).then(function(bien){

			$scope.estadoLlamada = angular.copy(bien.data.estado);

			if($scope.estadoLlamada == "ok") {

				$scope.eventosC = angular.copy(bien.data.arrayEventos);
				$scope.participantes = angular.copy(bien.data.arrayParticipantes);
				$scope.participantes2 = angular.copy(bien.data.arrayParticipantes);
				$scope.infoEventos = angular.copy(bien.data.infoEventos);

				$scope.eventosC.forEach(function(valor,indice){
					if(valor["fecha"] != "no") {
						valor["fecha"] = new Date(valor["fecha"]);
						valor["fecha"] = $scope.ceros(valor["fecha"].getDate())+"-"+$scope.ceros(valor["fecha"].getMonth()+1)+"-"+valor["fecha"].getFullYear();
					} else {
						 valor["fechasPosibles"].forEach(function(datoFecha,key){
						 	datoFecha["fecha"] = new Date(datoFecha["fecha"]);
						 	datoFecha["fecha"] = $scope.ceros(datoFecha["fecha"].getDate())+"-"+$scope.ceros(datoFecha["fecha"].getMonth())+"-"+datoFecha["fecha"].getFullYear();
						 }); 
					 }
					valor['aforo'] = parseInt(valor['aforo']);
					valor['entradasDisponibles'] = parseInt(valor['entradasDisponibles']);
					valor['precioEntrada'] = parseFloat(valor['precioEntrada']);
				});

				$scope.idsEvents = [];

				$scope.infoEventos.forEach(function(valor,indice){
					valor["id"] = parseInt(valor["id"]);
					$scope.idsEvents[valor["nombre"].toString()] = valor["id"];		
				});

			} else {

				 $scope.muestraMens(bien.data.tipoError);

			 }
		},
		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.datoss();

	$scope.anhadir = function() {

		$scope.datos = {
					'id'                  : null,
					'nombre'              : null,
					'descripcion'         : null,
					'rutaImagen'          : null,
					'lugar' 		      : null,
					'fecha'               : null,
					'aforo'               : null,
					'precioEntrada'       : null,
					'entradasDisponibles' : null,
					'arrayParticipantes'  : [],
					'newFechasP'          : []
				};

		$scope.modal = {
			'titulo'   : ' Añadir Nuevo Evento',
			'icono'    : 'note_add',
			'hColor'   : 'green lighten-4',
			'rCampos'  : '',
			'boton'    : 'Añadir',
			'bIcono'   : 'save',
			'ocultar'  : '',
			'btTipo'   : 'boton-verde',
			'datos'    : $scope.datos

 		};

 		$scope.ocultarPro = "";
 		$scope.productoElegido = "";
 		$scope.opcionAdd = "a";
 		$scope.tipoInsercion();
 		$scope.ocultarD = "display:none;";
 		$scope.ocultarH = "display:none;";
 		$scope.ocultarJ = "";
	}

	$scope.modif = function() {
		$scope.miThis = this;
		$scope.CopE = angular.copy($scope.miThis.evento);

		if($scope.CopE.fechasPosibles != null) {
			$scope.CopE.fechasPosibles.forEach(function(valor,indice) {
				$scope.CopE.fechasPosibles[indice] = new Date(valor[0]);	
			});
		}

		$scope.CopE.arrayParticipantes.forEach(function(valor,indice) {
			$scope.CopE.arrayParticipantes[indice] = {'id':valor[0],'nombre':valor[1],'apellido1':valor[2],'apellido2':valor[3],'grupo':valor[4]};	
		});

		$scope.datos = {
					'id'                  : $scope.CopE.id,
					'nombre'              : $scope.CopE.nombre,
					'descripcion'         : $scope.CopE.descripcion,
					'rutaImagen'          : $scope.CopE.rutaImagen,
					'lugar' 		      : $scope.CopE.lugar[0],
					'fecha'               : $scope.CopE.fecha,
					'aforo'               : $scope.CopE.aforo,
					'precioEntrada'       : $scope.CopE.precioEntrada,
					'entradasDisponibles' : $scope.CopE.entradasDisponibles,
					'arrayParticipantes'  : $scope.CopE.arrayParticipantes,
					'newFechasP'          : $scope.CopE.fechasPosibles

				};

		$scope.modal = {
			'titulo'   : ' Acciones sobre '+$scope.datos.nombre,
			'icono'    : 'build',
			'hColor'   : 'indigo lighten-4',
			'rCampos'  : 'resalte-campos',
			'qeHacer'  : '',
			'boton'    : 'Guardar',
			'bIcono'   : 'save',
			'btTipo'   : 'boton-azul',
			'ocultar'  : 'display:none;',
			'datos'    : $scope.datos

 		};
 		$scope.ocultarA = "display:none;";
		$scope.ocultarJ = "display:none;";
		$scope.ocultarC = "display:none;";
		$scope.ocultarH = "";
		$scope.opcionMod = "";
	}

	$scope.newFecha = function() {
		$scope.comprobarF = false;
		$scope.miThiss = this;

		$scope.datos.newFechasP.forEach(function(laaFecha,laKey) {
			if (laaFecha.getTime() == $scope.laNuevaFecha.getTime()) {
				$scope.comprobarF = true;
			}
		});

		if ($scope.comprobarF) {
			alert("no se puede añadir la misma fecha dos veces");
		} else {
			 $scope.datos.newFechasP.push($scope.miThiss.laNuevaFecha);
		 }
	}

	$scope.borraFecha = function() {
		$scope.miThiss = this;

		$scope.datos.newFechasP.forEach(function(laaFecha,laKey) {
			if (laaFecha.getTime() == $scope.miThiss.fechaP.getTime()) {
				$scope.datos.newFechasP.splice(laKey, 1);
			}
		});
	}

	$scope.queHacer = function() {
		switch ($scope.modal.boton) {
			case "Añadir":
				$scope.anhadirArriba();
			break;
			case "Guardar":
				if($scope.auxiliar) {
					$scope.modificarArriba();
				} else {
					 $scope.modiPartiArriba();
				 }
			break;
			default:
				alert("Error");
			break;
		}
	}

	$scope.newParticipante = function() {
		$scope.comprobarF = false;
		$scope.aux4 = $scope.partyElegido.split(" - ");
		$scope.aux5 = $scope.aux4[1].split(" ");
		$scope.miParty = {'id':$scope.aux4[0],'nombre':$scope.aux5[0],'apellido1':$scope.aux5[1],'apellido2':$scope.aux5[2],'grupo':$scope.aux4[2]};		

		$scope.datos.arrayParticipantes.forEach(function(elPart,laKey) {
			if (elPart.id == $scope.miParty.id) {
				$scope.comprobarF = true;
			}
		});

		if ($scope.comprobarF) {
			alert("no se puede añadir el mismo partcipante dos veces");
		} else {
			 $scope.datos.arrayParticipantes.push($scope.miParty);
		 }
	}

	$scope.borraParticip = function() {
		$scope.miThiss = this;
		$scope.idP = $scope.miThiss.elParty.id;
		$scope.datos.arrayParticipantes.forEach(function(elPart,laKey) {
			if (elPart.id == $scope.idP) {
				$scope.datos.arrayParticipantes.splice(laKey, 1);
			}
		});
	}

	$scope.tipoInsercion = function() {
		switch ($scope.opcionAdd) {
			case "a":
			$scope.ocultarA = "display:none;";
			$scope.ocultarB = "";
			$scope.ocultarC = "";
			$scope.ocultarG = "";
			break;
			case "b":
			$scope.ocultarA = "display:none;";
			$scope.ocultarB = "";
			$scope.ocultarC = "display:none;";
			$scope.ocultarG = "display:none;";
			break;
			case "c":
			$scope.ocultarA = "";
			$scope.ocultarB = "display:none;";
			$scope.ocultarC = "";
			$scope.ocultarG = "";
			break;
			default:
				alert("Error");
			break;
		}
	}

	$scope.tipoModificacion = function() {
		switch ($scope.opcionMod) {
			case "m":
				$scope.ocultarJ = "";
				$scope.ocultarC = "display:none;";
				$scope.auxiliar = true;
			break;
			case "p":
				$scope.ocultarJ = "display:none;";
				$scope.ocultarC = "";
				$scope.auxiliar = false;
			break;
			default:
				alert("Error");
			break;
		}
	}

	$scope.formatearFecha = function(auxF) {
		return auxF.getFullYear()+"-"+$scope.ceros(auxF.getMonth()+1)+"-"+$scope.ceros(auxF.getDate());
	}

	$scope.formatearFecha2 = function(fechaAux) {
		$scope.fechaAux2 = fechaAux.split("-");
		return $scope.fechaAux2[2]+"-"+$scope.fechaAux2[1]+"-"+$scope.fechaAux2[0];
	}

	$scope.participantee = function(id) {
		$scope.varAux=[];
		$scope.participantes2.forEach(function(elParty,laKey) {	
			if (id == elParty["id"]) {
				$scope.varAux = $scope.participantes2[laKey];
			}
		});

		return $scope.varAux;
	}

	$scope.participantesArray = function(parti) {
		$scope.resultado = [];
		parti.forEach(function(elParty,laKey) {
			$scope.resultado.push($scope.participantee(elParty["id"]));
		});

		return $scope.resultado;
	}

	$scope.anhadirArriba = function() {
		$scope.datosP = angular.copy($scope.modal.datos);
		auxF = angular.copy($scope.modal.datos.fecha);
		if($scope.opcionAdd == "c") {
			$scope.datosP.id = $scope.idsEvents[$scope.eventoElegido];
		} else {
			 $scope.datosP.id = null;
		 }

		$scope.datosP.newFechasP.forEach(function(laaFecha,laKey) {
			$scope.datosP.newFechasP[laKey] = $scope.formatearFecha(laaFecha);
		});

		$scope.auxPartiis = $scope.participantesArray($scope.datosP.arrayParticipantes);

		var peticion = {
		"url"    : "php/funcionalidades/gestionar.php",
		"method" : "POST",
		"data"   : { opcion     : "evento",
					 accion     : "anhadir",
					 parametros :  { 'id'                  : $scope.datosP.id,
					 				 'nombre'              : $scope.datosP.nombre,
					 				 'descripcion'         : $scope.datosP.descripcion,
					 			     'rutaImagen'          : $scope.datosP.rutaImagen,
					 				 'lugar'               : $scope.datosP.lugar,
					 				 'fecha'               : $scope.datosP.fecha,
					 				 'aforo'               : $scope.datosP.aforo,
					 				 'precio'              : $scope.datosP.precioEntrada,
					 				 'entradasDisponibles' : $scope.datosP.entradasDisponibles,
					 				 'fechasPosibles'      : $scope.datosP.newFechasP,
					 				 'arrayParticipantes'  : $scope.auxPartiis
					 				}
					 		 

					 			  
				   }
		};

		$http(peticion).then(function(bien){
		$scope.respuesta = angular.copy(bien.data);
			if($scope.respuesta.estado == undefined) {
				$scope.muestraMens("Error desconocido");
			} else {
				 if($scope.respuesta.estado == "ok") {				
					 $scope.datoss();
					 $scope.cerrarModal('#mymodal1');
					 $scope.muestraMens($scope.respuesta.mens);	
				  } else {
				  	  $scope.cerrarModal('#mymodal1');
				  	  $scope.muestraMens($scope.respuesta.tipoError);
			 	   }
			 }
		},

		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.modificarArriba = function() {

		$scope.datosEvMod = angular.copy($scope.modal.datos);

		$scope.datosEvMod.newFechasP.forEach(function(laaFecha,laKey) {
			$scope.datosEvMod.newFechasP[laKey] = $scope.formatearFecha(laaFecha);
		});

		$scope.auxxPartis = $scope.participantesArray($scope.datosEvMod.arrayParticipantes);

		if($scope.datosEvMod.fecha == "no"){
			$scope.datosEvMod.fecha = null;
		} else {
		    $scope.datosEvMod.fecha = $scope.formatearFecha2($scope.datosEvMod.fecha);
	     }	

		var peticion = {
		"url"    : "php/funcionalidades/gestionar.php",
		"method" : "POST",
		"data"   : { opcion     : "evento",
					 accion     : "modif",
					 parametros : {  'id'                  : parseInt($scope.datosEvMod.id),
					 				 'nombre'              : $scope.datosEvMod.nombre,
					 				 'descripcion'         : $scope.datosEvMod.descripcion,
					 			     'rutaImagen'          : $scope.datosEvMod.rutaImagen,
					 				 'lugar'               : $scope.datosEvMod.lugar,
					 				 'fecha'               : $scope.datosEvMod.fecha,
					 				 'arrayParticipantes'  : $scope.auxxPartis,
					 				 'aforo'               : $scope.datosEvMod.aforo,
					 				 'precio'              : $scope.datosEvMod.precioEntrada,
					 				 'entradasDisponibles' : $scope.datosEvMod.entradasDisponibles,
					 				 'fechasPosibles'      : $scope.datosEvMod.newFechasP
					 				}
				   }
		};

		$http(peticion).then(function(bien){
		$scope.respuesta = angular.copy(bien.data);
			if($scope.respuesta.estado == undefined) {
				alert("Error desconocido");
			} else {
				 if($scope.respuesta.estado == "ok") {				
					 $scope.datoss();
					 $scope.cerrarModal('#mymodal1');
					 $scope.muestraMens($scope.respuesta.mens);	
				  } else {
				  	  $scope.cerrarModal('#mymodal1');
				  	  $scope.muestraMens($scope.respuesta.tipoError);
			 	   }
			 }
		},

		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.modiPartiArriba = function() {

		$scope.datosEvModParts = angular.copy($scope.modal.datos);
		$scope.auxPartis = $scope.participantesArray($scope.datosEvModParts.arrayParticipantes);

		var peticion = {
		"url"    : "php/funcionalidades/gestionar.php",
		"method" : "POST",
		"data"   : { opcion     : "evento",
					 accion     : "modifParti",
					 parametros : {  'id'                  : parseInt($scope.datosEvModParts.id),
					 				 'nombre'              : $scope.datosEvModParts.nombre,
					 				 'descripcion'         : $scope.datosEvModParts.descripcion,
					 			     'rutaImagen'          : $scope.datosEvModParts.rutaImagen,
					 				 'lugar'               : $scope.datosEvModParts.lugar,
					 				 'fecha'               : $scope.datosEvModParts.fecha,
					 				 'aforo'               : $scope.datosEvModParts.aforo,
					 				 'precio'              : $scope.datosEvModParts.precioEntrada,
					 				 'entradasDisponibles' : $scope.datosEvModParts.entradasDisponibles,
					 				 'fechasPosibles'      : $scope.datosEvModParts.newFechasP,
					 				 'arrayParticipantes'  : $scope.auxPartis
					 				}
				   }
		};

		$http(peticion).then(function(bien){
		$scope.respuesta = angular.copy(bien.data);
			if($scope.respuesta.estado == undefined) {
				alert("Error desconocido");
			} else {
				 if($scope.respuesta.estado == "ok") {				
					 $scope.datoss();
					 $scope.cerrarModal('#mymodal1');
					 $scope.muestraMens($scope.respuesta.mens);	
				  } else {
				  	  $scope.cerrarModal('#mymodal1');
				  	  $scope.muestraMens($scope.respuesta.tipoError);
			 	   }
			 }
		},

		function(mal){
			$scope.muestraMens("Error en conexión");
		});
	}

	$scope.cerrarModal = function(referencia) {
		$(referencia).modal('close');
	}


}).

component('gestionarEvs', {
	templateUrl: 'js/vistas/geEve.view.html',
	controller: 'eventGestCtrl'
});