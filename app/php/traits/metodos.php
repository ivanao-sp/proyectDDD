<?php namespace metodosTraits;

trait metodos {

	function alta($conexion) {

		switch(get_class()) {

		    case 'productoClases\producto':

			// utilizamos la funcion jsonSerialize() tanto paras pasar a json como para extraer las varibles sin convertir y usarlas en php. Estas vienen en un array
		    $arrayDatos = $this->jsonSerialize();

			$conexion->beginTransaction();

		    // vamos a comprobar si hay que insertar en la tabla producto, productoTienda o en ambas 
		    
		    if($arrayDatos["nombre"] != null) {

		    	// cambiar nombre de categoria por id  		
		    	$result = \consultasClases\consultas::hacerSelect($conexion, "id", "categoria", " where nombre=?", array($arrayDatos["categoria"]));

		    	// si no esta la añadimos
		    	if(count($result) == 0) {
	  				\consultasClases\consultas::hacerInsert($conexion, "categoria", array(null,$arrayDatos["categoria"]));

	  				$arrayDatos["categoria"] = $conexion->query("SELECT LAST_INSERT_ID()")->fetch()[0];

		   		} else if (count($result) > 1) {
	  			 		throw new Exception('Error en la Base de Datos. Contacte con el servicio tecnico');
	  		 	 } else {
	  		 	 	 $arrayDatos["categoria"] = $result[0]["id"];
	  		 	  }

		   		// comprobar que no existe un tipo de producto con el mismo nombre
	  			$result = \consultasClases\consultas::hacerSelect($conexion, "*", "producto", " where nombre=?", array($arrayDatos["nombre"]));

		    	if(count($result) > 0) {
		    		throw new Exception('El tipo de producto ya esta añadido');
		    	}	

		   		\consultasClases\consultas::hacerInsert($conexion, "producto", array(null,$arrayDatos["nombre"],$arrayDatos["descripcion"],$arrayDatos["categoria"],$arrayDatos["fechaFinCampanha"]));

		   		$arrayDatos["id"] = $conexion->query("SELECT LAST_INSERT_ID()")->fetch()[0];
		   		if($arrayDatos["arrayIma"] != null) {
		   			// introduce todas los registros en la tabla imagenesProd
					foreach ($arrayDatos["arrayIma"] as $imagen) {
						\consultasClases\consultas::hacerInsert($conexion, "imagenesProd", array(null,$arrayDatos["id"],$imagen["ruta"]));
					}
		   		}
			}

			if($arrayDatos["stock"] != null) {

				// combrobar si existe la tienda 		
		    	$result = \consultasClases\consultas::hacerSelect($conexion, "*", "tienda", " where id=?", array($arrayDatos["idTienda"]));

		    	if(count($result) == 0) {
	  				throw new Exception('La tienda no existe');
		   		}

				\consultasClases\consultas::hacerInsert($conexion, "productoTienda", array(null,$arrayDatos["id"],$arrayDatos["idTienda"],$arrayDatos["stock"],$arrayDatos["precio"],1));
			}

			$conexion->commit();

			return array(
					'estado'=>"ok",
					'mens'=>"Producto Añadido Correctamente",
					);
		    break;

		    case 'eventoClases\evento':
		    	
		    	$arrayDatos = $this->jsonSerialize();

		    	$conexion->beginTransaction();
		    	
		    	// vamos a comprobar si hay que insertar en la tabla evento, eventoLugar o en ambas 

				if($arrayDatos["nombre"] != null) {

					// comprobar que no existe un tipo de evento con el mismo nombre
					$result = \consultasClases\consultas::hacerSelect($conexion, "*", "evento", " where nombre=?", array($arrayDatos["nombre"]));

		    		if(count($result) > 0) {
		    			throw new \Exception('El tipo de evento ya esta añadido');
		    		}

		    		\consultasClases\consultas::hacerInsert($conexion, "evento", array(null,$arrayDatos["nombre"],$arrayDatos["descripcion"],$arrayDatos["rutaImagen"]));

		    		$arrayDatos["id"] = $conexion->query("SELECT LAST_INSERT_ID()")->fetch()[0];
				}

				if($arrayDatos["aforo"] != null) {
	
					$resultL = \consultasClases\consultas::hacerSelect($conexion, "id", "lugar", " where lugar=?", array($arrayDatos["lugar"]));

					// si no existe lo creamos
					if(count($resultL) == 0) {
		    			\consultasClases\consultas::hacerInsert($conexion, "lugar", array(null,$arrayDatos["lugar"]));
		    			$arrayDatos["lugar"] = $conexion->query("SELECT LAST_INSERT_ID()")->fetch()[0];
		    		} else {
		    			 $arrayDatos["lugar"] = $resultL->fetch()[0];
		    		 }

		    		\consultasClases\consultas::hacerInsert($conexion, "eventoLugar", array(null,$arrayDatos["id"],$arrayDatos["lugar"],null,$arrayDatos["aforo"],$arrayDatos["precioEntrada"],$arrayDatos["entradasDisponibles"]));

		    		$nuevaId = $conexion->query("SELECT LAST_INSERT_ID()")->fetch()[0];

		    		foreach ($arrayDatos["fechasPosibles"] as $laFecha) {
		    			\consultasClases\consultas::hacerInsert($conexion, "fechasPosibles", array($nuevaId,$laFecha));
		    		}

		    		$conexion->commit();

		    		return $nuevaId;		    		 
				} else {
					 $conexion->commit();

					 return "noPartis";
				 }    	

			break;   

		    default:
		    	throw new \Exception('tipo de clase erronea');
			break;
		}

	}

	function baja($conexion) {

		switch(get_class()) {

			case 'productoClases\producto':

			$conexion->beginTransaction();

			\consultasClases\consultas::hacerUpdate($conexion, "productoTienda", array("estado"), array(4,$this->jsonSerialize()["id"]), "where idProductoTienda=?");

			$conexion->commit();

			return array(
					'estado'=>"ok",
					'mens'=>"Producto retirado Correctamente",
					);

	 		break;

	  		default:
		    	throw new \Exception('tipo de clase erronea');
			break;
		}
	}	

	function modificar($conexion) {

		switch(get_class()) {

			case 'productoClases\producto':

			$arrayDatos = $this->jsonSerialize();

			$conexion->beginTransaction();
			
			// comprobar si existe un ProductoTienda con esa id
			$result = \consultasClases\consultas::hacerSelect($conexion, "*", "productoTienda", " where idProductoTienda=?", array($arrayDatos["id"]));
			if(count($result) == 0) {
				throw new \Exception('No existe un producto con esa id');
			}

			// obtener id de producto
			$idProd = \consultasClases\consultas::hacerSelect($conexion, "idProd", "productoTienda", " where idProductoTienda=?", array($arrayDatos["id"]))[0]["idProd"];

			// cambiar nombre de categoria por id  		
		    $result = \consultasClases\consultas::hacerSelect($conexion, "id", "categoria", " where nombre=?", array($arrayDatos["categoria"]));

		    // si no esta la añadimos
		    if(count($result) == 0) {
	  			\consultasClases\consultas::hacerInsert($conexion, "categoria", array(null,$arrayDatos["categoria"]));

	  			$arrayDatos["categoria"] = $conexion->query("SELECT LAST_INSERT_ID()")->fetch()[0];

		   	} else if (count($result) > 1) {
	  			 throw new \Exception('Error en la Base de Datos. Contacte con el servicio tecnico');
	  		 } else {
	  		 	  $arrayDatos["categoria"] = $result[0]["id"];
	  		  }

		   	// comprobar que no existe un producto con el mismo nombre
	  		$result = \consultasClases\consultas::hacerSelect($conexion, "*", "producto", " where nombre=?", array($arrayDatos["nombre"]));

		    if(count($result) > 0) {
		    	$result = \consultasClases\consultas::hacerSelect($conexion, "*", "producto", " where nombre=? and id=?", array($arrayDatos["nombre"],$idProd));
		    	if(count($result) != 1) {
		    		throw new \Exception('Dos productos no puden llamarse igual');
		    	}

		    }

			\consultasClases\consultas::hacerUpdate($conexion, "producto", array("nombre","descripcion","categoria","fechaFinCampanha"), array($arrayDatos["nombre"],$arrayDatos["descripcion"],$arrayDatos["categoria"],$arrayDatos["fechaFinCampanha"],$idProd), "where id=?");

			// comprobamos si alguna imagen fue borrada y borrarla si procede, despues comprobar si alguna fue añadida(porque su id es null) y finalmente si fueron modicadas
		   	$aux = false;
		    $result = \consultasClases\consultas::hacerSelect($conexion, "*", "imagenesProd", " where idProd=?", array($idProd));

		    for ($i=0; $i < count($result); $i++) {
		    	$aux = false;

		    	foreach ($arrayDatos["arrayIma"] as $key => $registro) {

		    		if($registro->id == null) {
		    			$result2 = \consultasClases\consultas::hacerSelect($conexion, "*", "imagenesProd", " where ruta=?", array($registro->ruta));
		    			if(count($result2) != 0) {
		    				throw new \Exception('No se puede añadir la misma imagen dos veces');
		    			}

						\consultasClases\consultas::hacerInsert($conexion, "imagenesProd", array(null,$idProd,$registro->ruta));
						$resultado2 = $conexion->query("SELECT LAST_INSERT_ID()");
		    			$arrayDatos["arrayIma"][$key]["id"] = $resultado2->fetch()[0];
						
					} elseif($result[$i]["id"] == intval($registro->id)) {
		    			 $aux = true;

		    			 if($result[$i]["ruta"] != $registro->ruta) {

		    				 \consultasClases\consultas::hacerUpdate($conexion, "imagenesProd", array("ruta"), array($registro->ruta,$result[$i]["id"]), "where id=?");
		    			 }
		    		 }
				}

				if(!$aux) {
					\consultasClases\consultas::hacerDelete($conexion, "imagenesProd", " where id=?", array($result[$i]["id"]));
				}		    	
		    }			

			// conseguir id de estado

			if($arrayDatos["stock"] == 0) {

		   		$arrayDatos["estado"] = 3;
		   	} else {

		   		 $result = \consultasClases\consultas::hacerSelect($conexion, "id", "estado", " where nombre=?", array($arrayDatos["estado"]));

		    	 if(count($result) != 1) {
	  				 throw new \Exception('Error en la Base de Datos. Contacte con el servicio tecnico');
		   		 } 

		   		 $arrayDatos["estado"] = $result[0]["id"];
		   	 }

			\consultasClases\consultas::hacerUpdate($conexion, "productoTienda", array("stock","precio","estado"), array($arrayDatos["stock"],$arrayDatos["precio"],$arrayDatos["estado"],$arrayDatos["id"]), "where idProductoTienda=?");

		    $conexion->commit();

		    return array(
					'estado'=>"ok",
					'mens'=>"Producto Modificado Correctamente",
					);

			break;

			case 'eventoClases\evento':

				$arrayDatos = $this->jsonSerialize();

				$conexion->beginTransaction();

		    	// comprobar que existe un evento con esas id
		    	$resultado = \consultasClases\consultas::hacerSelect($conexion, "*", "eventoLugar", " where idEventoLugar=?", array($arrayDatos["id"]));

		    	if(count($resultado) == 0) {
			    	throw new \Exception('No hay ningun evento con esa id');
			    }

			    $idTaEve = \consultasClases\consultas::hacerSelect($conexion, "idEvento", "eventoLugar", " where idEventoLugar=?", array($arrayDatos["id"]))[0][0];

			    \consultasClases\consultas::hacerUpdate($conexion, "evento", array("nombre","descripcion","rutaImagen"), array($arrayDatos["nombre"],$arrayDatos["descripcion"],$arrayDatos["rutaImagen"],$idTaEve), " where id=?");

			    \consultasClases\consultas::hacerUpdate($conexion, "eventoLugar", array("fecha","aforo","precioEntrada","entradasDisponibles"), array($arrayDatos["fecha"],$arrayDatos["aforo"],$arrayDatos["precioEntrada"],$arrayDatos["entradasDisponibles"],$arrayDatos["id"]), " where idEventoLugar=?");

			    \consultasClases\consultas::hacerDelete($conexion, "fechasPosibles", " where idEvento=?", array($arrayDatos["id"]));		

			    foreach ($arrayDatos["fechasPosibles"] as $fechaSelect) {
			    	\consultasClases\consultas::hacerInsert($conexion, "fechasPosibles", array($arrayDatos["id"],$fechaSelect));
				}

				$conexion->commit();

				return array(
						'estado'=>"ok",
						'mens'=>"Evento Modificado Correctamente",
						);

			break;

			case 'usuarioClases\usuario':

			$datos = $this->jsonSerialize();

			$campos = array_keys($datos);

			\consultasClases\consultas::hacerUpdate($conexion, "usuario", array("NIF","nombre","apellido1","apellido2","telefono","email","direccion","localidad","provincia","password"), array($datos["NIF"],$datos["nombre"],$datos["apellido1"],$datos["apellido2"],$datos["telefono"],$datos["email"],$datos["direccion"],$datos["localidad"],$datos["provincia"],$datos["password"],$datos["id"]), " where id=?");

				return array(
						'estado'=>"ok",
						'mens'=>"Perfil Modificado Correctamente",
						);

			break;

			default:
		    	throw new \Exception('tipo de clase erronea');
			break;
		}

	}

}

?>
