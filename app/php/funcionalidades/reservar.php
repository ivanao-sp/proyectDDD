<?php

try {

	session_start();

	if($_SESSION['permisos'] != 3) {

		throw new Exception('Accion no permitida');

	} else {

		 require '../clases/conectBd.php';
		 require '../clases/consultas.php';
		 require '../clases/cosa.php';
		 require '../interfaces/metodos.php';
		 require '../traits/metodos.php';
		 require '../clases/producto.php';
		 require '../clases/evento.php';
		 require '../clases/persona.php';
		 require '../clases/participante.php';
		 require '../clases/tienda.php';
		 require '../clases/reserva.php';
		 require '../clases/reservaProd.php';
		 require '../clases/reservaEvent.php';

		 $json = file_get_contents('php://input');
		 $res = json_decode($json);
      	 $opcion = $res->opcion;
      	 $datosSubidos = $res->parametros;

	     $conexion = \conectBdClases\conectBd::abrirConexion();

	     switch($opcion) {

			 case "producto":

			 	 reservarProd($conexion,$datosSubidos);	

			 break;

			 case "evento":	

				 reservarEvent($conexion,$datosSubidos);

			 break;

			 default:
				 throw new Exception('Error en Servidor');
			 break;
			
	     }
	 }
	
}

catch (PDOException $e) {

		$resp = array(
					"estado"=>"fallo",
					"tipoError"=>$e->getMessage(),
					);

		echo json_encode($resp);
}

catch (Exception $e) {
	
		$res = array(
					'estado'=>"fallo",
					'tipoError'=>$e->getMessage(),
					);

		$conexion = null;

		echo json_encode($res);
}

function reservarProd($conexion,$datosSubidos) {

	$idTienda = \consultasClases\consultas::hacerSelect($conexion, "idTienda", "productoTienda", " where idProductoTienda=?", array(intval($datosSubidos->prod)))[0]["idTienda"];

	$idP = \consultasClases\consultas::hacerSelect($conexion, "idProd", "productoTienda", " where idProductoTienda=?", array(intval($datosSubidos->prod)))[0]["idProd"];

	$precio = \consultasClases\consultas::hacerSelect($conexion, "precio", "productoTienda", " where idProductoTienda=?", array(intval($datosSubidos->prod)))[0]["precio"];

	$datosReserva = array();
	$datosReserva["idReserva"] = null;
	$datosReserva["idUsuario"] = $_SESSION['id'];
	$datosReserva["cantidad"] = $datosSubidos->cant;
	$datosReserva["precioTotal"] = $precio*$datosSubidos->cant;
	$datosReserva["fecha"] = null;
	$datosReserva["idProducto"] = $idP;
	$datosReserva["idTienda"] = $idTienda;

	$reservaIni = new \reservaProdClases\reservaProd($datosReserva);

	$datosDeLaReserva = $reservaIni->jsonSerialize();

	$insertReserva = 'INSERT INTO `reservaProd` VALUES (null,?,?,?,?,?,CURRENT_TIMESTAMP)';

	$insertReservaPrepa = $conexion->prepare($insertReserva);
	$insertReservaPrepa->execute(array($datosDeLaReserva["idUsuario"],$datosDeLaReserva["idProducto"],$datosDeLaReserva["idTienda"],$datosDeLaReserva["cantidad"],$datosDeLaReserva["precioTotal"]));

	$stockActual = \consultasClases\consultas::hacerSelect($conexion, "stock", "productoTienda", " where idProductoTienda=?", array(intval($datosSubidos->prod)))[0]["stock"];

	$stockActual = $stockActual - $datosSubidos->cant;

	\consultasClases\consultas::hacerUpdate($conexion, "productoTienda", array("stock"), array($stockActual,$datosSubidos->prod), "where idProductoTienda=?");

	$respuesta = array('estado'=>"ok",'mens'=>"Reserva realizada Correctamente, recoja su pedido en la Tienda");

	echo json_encode($respuesta);
}

function reservarEvent($conexion,$datosSubidos) {

	$precio = \consultasClases\consultas::hacerSelect($conexion, "precioEntrada", "eventoLugar", " where idEventoLugar=?", array(intval($datosSubidos->event)))[0]["precioEntrada"];

	$datosReserva = array();
	$datosReserva["idReserva"] = null;
	$datosReserva["idUsuario"] = $_SESSION['id'];
	$datosReserva["cantidad"] = $datosSubidos->cant;
	$datosReserva["precioTotal"] = $precio*$datosSubidos->cant;
	$datosReserva["fecha"] = null;
	$datosReserva["idEvento"] = $datosSubidos->event;

	$reservaIni = new \reservaEventClases\reservaEvent($datosReserva);

	$datosDeLaReserva = $reservaIni->jsonSerialize();

	$insertReserva = 'INSERT INTO `reservaEvent` VALUES (null,?,?,?,?,CURRENT_TIMESTAMP)';

	$insertReservaPrepa = $conexion->prepare($insertReserva);
	$insertReservaPrepa->execute(array($datosDeLaReserva["idUsuario"],$datosDeLaReserva["idEvento"],$datosDeLaReserva["cantidad"],$datosDeLaReserva["precioTotal"]));

	$EdispActual = \consultasClases\consultas::hacerSelect($conexion, "entradasDisponibles", "eventoLugar", " where idEventoLugar=?", array(intval($datosSubidos->event)))[0]["entradasDisponibles"];

	$EdispActual = $EdispActual - $datosSubidos->cant;

	\consultasClases\consultas::hacerUpdate($conexion, "eventoLugar", array("entradasDisponibles"), array($EdispActual,$datosSubidos->event), "where idEventoLugar=?");

	$respuesta = array('estado'=>"ok",'mens'=>"Reserva realizada Correctamente, que disfrute del Evento");

	echo json_encode($respuesta);
}


?>