<?php

try {

	session_start();

	if(isset($_SESSION['permisos']) && $_SESSION['permisos'] != 3) {

		throw new Exception('Accion no permitida');

	} else {

		 require '../clases/conectBd.php';
		 require '../clases/consultas.php';
		 require '../clases/cosa.php';
		 require '../interfaces/metodos.php';
		 require '../traits/metodos.php';
		 require '../clases/producto.php';
		 require '../clases/evento.php';
		 require '../clases/persona.php';
		 require '../clases/participante.php';
		 require '../clases/tienda.php';

		 $json = file_get_contents('php://input');
		 $resp = json_decode($json);
      	 $opcion = $resp->opcion;

	     $conexion = \conectBdClases\conectBd::abrirConexion();

	     switch($opcion) {

			 case "productos":

			 	 verProductos($conexion);	

			 break;

			 case "eventos":	

				 verEventos($conexion,$resp);

			 break;

			 default:
				 throw new Exception('Error en Servidor');
			 break;
			
	     }
	 }
	
}

catch (PDOException $e) {

		$resp = array(
					"estado"=>"fallo",
					"tipoError"=>$e->getMessage(),
					);

		echo json_encode($resp);
}

catch (Exception $e) {
	
		$res = array(
					'estado'=>"fallo",
					'tipoError'=>$e->getMessage(),
					);

		$conexion = null;

		echo json_encode($res);
}

// Funciones PHP

function verProductos($conexion) {

	$tProductos = $conexion->query("SELECT * from producto")->fetchAll();
	$arrayProductos = array();

	foreach ($tProductos as $prodSelec) {

		$categoriaN = $conexion->query("SELECT nombre from categoria where id=".$prodSelec["categoria"])->fetch()[0];

		$selectImagenes = $conexion->query("SELECT * from imagenesProd where idProd=".$prodSelec["id"])->fetchAll();
		$arrayIma = array();

		foreach ($selectImagenes as $imagen) {

			$arrayFilaIma = array();
			$arrayFilaIma["id"] = $imagen["id"];
			$arrayFilaIma["ruta"] = $imagen["ruta"];
			$arrayIma[] = $arrayFilaIma;
		}

		$tProductoT = $conexion->query("SELECT * from productoTienda where idProd=".$prodSelec["id"])->fetchAll();

		foreach ($tProductoT as $proTiSelec) {

			$estadoN = $conexion->query("SELECT nombre from estado where id=".$proTiSelec["estado"])->fetch()[0];

			$auxProd = array();

			$auxProd["id"] = $proTiSelec["idProductoTienda"];
			$auxProd["nombre"] = $prodSelec["nombre"];
			$auxProd["descripcion"] = $prodSelec["descripcion"];
			$auxProd["categoria"] = $categoriaN;
			$auxProd["fecha"] = $prodSelec["fechaFinCampanha"];
			$auxProd["arrayIma"] = $arrayIma;
			$auxProd["idTienda"] = $proTiSelec["idTienda"];
			$auxProd["stock"] = $proTiSelec["stock"];
			$auxProd["precio"] = $proTiSelec["precio"];
			$auxProd["estado"] = $estadoN;

			$prodInicializado = new \productoClases\producto($auxProd);

			if($prodInicializado->jsonSerialize()["estado"] == "Aceptado") {

				$arrayProductos[] = $prodInicializado;

			}
		}
	}

	$arrayRespuesta["arrayProds"] = $arrayProductos;

	$arrayRespuesta["arrayTiendas"] = verTiendas($conexion);

	$arrayRespuesta["categorias"] = $conexion->query("SELECT * from categoria")->fetchAll();

	$arrayRespuesta["estado"] = "ok";

	echo json_encode($arrayRespuesta); 	

}

function verEventos($conexion,$reci) {
		
	$tEventos = $conexion->query("SELECT * from evento")->fetchAll();
	$arrayEventos = array();

	foreach ($tEventos as $regTaEvent) {
	
		$consultaEvLu = 'SELECT * from eventoLugar where idEvento='.$regTaEvent["id"];

		$selectConstulta = $conexion->prepare($consultaEvLu);

		$selectConstulta->execute(array($regTaEvent["id"]));

		$tEventLugar = $selectConstulta->fetchAll();

		foreach ($tEventLugar as $regTaEveLugar) {

			$fposibles = $conexion->query("SELECT fecha from fechasPosibles where idEvento=".$regTaEveLugar["idEventoLugar"])->fetchAll();

			$lugar = $conexion->query("SELECT lugar from lugar where id=".$regTaEveLugar["idLugar"])->fetchAll()[0];

			$participantes = verParticipantes($conexion, $conexion->query("SELECT * from participante inner join eventoParticipante on participante.id = eventoParticipante.idParticipante where eventoParticipante.idEvento=".$regTaEveLugar["idEventoLugar"])->fetchAll());
			$arrayParticipantes = array();
			$aux="";

			foreach ($participantes as $party) {
				$datosParty = $party->jsonSerialize();
				if ($datosParty["grupo"] == null) {
					$aux="Sin grupo";
				} else {
					 $aux=$datosParty["grupo"];
				 }
				$arrayParticipantes[] = array($datosParty["id"],$datosParty["nombre"],$datosParty["apellido1"],$datosParty["apellido2"],$aux); 
			}

			$auxEvento = array();
			$auxEvento["id"] = $regTaEveLugar["idEventoLugar"];
			$auxEvento["nombre"] = $regTaEvent["nombre"];
			$auxEvento["descripcion"] = $regTaEvent["descripcion"];
			$auxEvento["rutaImagen"] = $regTaEvent["rutaImagen"];
			$auxEvento["lugar"] = $lugar;
			$auxEvento["fecha"] = $regTaEveLugar["fecha"];
			$auxEvento["fechasPosibles"] = $fposibles;
			$auxEvento["aforo"] = $regTaEveLugar["aforo"];
			$auxEvento["precio"] = $regTaEveLugar["precioEntrada"];
			$auxEvento["entradasDisponibles"] = $regTaEveLugar["entradasDisponibles"];
			$auxEvento["arrayParticipantes"] = $arrayParticipantes;

			$eventoIni = new \eventoClases\evento($auxEvento);

			if ($reci->limitacion1 == "no") {

				if($eventoIni->jsonSerialize()["fecha"] != null && strtotime($eventoIni->jsonSerialize()["fecha"]) > strtotime("now")) {

					$arrayEventos[] = $eventoIni;
				
			    }

			} elseif ($eventoIni->jsonSerialize()["fecha"] != null && (strtotime($eventoIni->jsonSerialize()["fecha"]) > strtotime($reci->limitacion1) &&  strtotime($eventoIni->jsonSerialize()["fecha"]) < strtotime($reci->limitacion2))) {
				$arrayEventos[] = $eventoIni;
			 } 

		}
		
	}

	$arrayRespuesta = array();
	$arrayRespuesta["arrayEventos"] = $arrayEventos;
	$arrayRespuesta["estado"] = "ok";

	echo json_encode($arrayRespuesta); 
}

function verTiendas($conexion) {

	$result = $conexion->query("SELECT * from tienda");
	$tiendas = $result->fetchAll();
	$arrayTiendas = array();

	foreach ($tiendas as $filatTien) {
		$auxTiend = array();

		$auxTiend['id'] = $filatTien['id'];
		$auxTiend['nombre'] = $filatTien['nombre'];
		$auxTiend['direccion'] = $filatTien['direccion'];
		$auxTiend['ciudad'] = $filatTien['ciudad'];
		$auxTiend['codigoPostal'] = $filatTien['codigoPostal'];
		$auxTiend['telefono'] = $filatTien['telefono'];
		$auxTiend['email'] = $filatTien['email'];
		$auxTiend['fax'] = $filatTien['fax'];

		$tiendaIni = new \tiendaClases\tienda($auxTiend);

		$arrayTiendas[] = $tiendaIni;
	}

	return $arrayTiendas;
}

function verParticipantes($conexion, $tParticipantes) {
	$arrayParticipantes = array();
	$elGrupo;

	foreach ($tParticipantes as $selectParti) {
		if($selectParti["grupo"] != null) {
			$elGrupo = $conexion->query("SELECT nombre from grupo where id=".$selectParti["grupo"])->fetchAll()[0]["nombre"];
		} else {
			 $elGrupo = "sin grupo";			
		 }

		$auxParticipante = array();

		$auxParticipante["id"] = $selectParti["id"];
		$auxParticipante["NIF"] = $selectParti["NIF"];
		$auxParticipante["nombre"] = $selectParti["nombre"];
		$auxParticipante["apellido1"] = $selectParti["apellido1"];
		$auxParticipante["apellido2"] = $selectParti["apellido2"];
		$auxParticipante["telefono"] = $selectParti["telefono"];
		$auxParticipante["email"] = $selectParti["email"];
		$auxParticipante["direccion"] = $selectParti["direccion"];
		$auxParticipante["localidad"] = $selectParti["localidad"];
		$auxParticipante["provincia"] = $selectParti["provincia"];
		$auxParticipante["grupo"] = $elGrupo;

		$partcipanteIni = new \participanteClases\participante($auxParticipante);

		$arrayParticipantes[] = $partcipanteIni;
	}

	return $arrayParticipantes;
}


?>
