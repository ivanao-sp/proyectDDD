<?php

try {

	session_start();

	if(!isset($_SESSION['permisos']) || $_SESSION['permisos'] != 1) {

		throw new Exception('Accion no permitida');

	} else {

		 require '../clases/conectBd.php';
		 require '../clases/consultas.php';
		 require '../clases/cosa.php';
		 require '../interfaces/metodos.php';
		 require '../traits/metodos.php';
		 require '../clases/producto.php';
		 require '../clases/evento.php';
		 require '../clases/persona.php';
		 require '../clases/participante.php';
		 require '../clases/tienda.php';

		 $json = file_get_contents('php://input');
		 $res = json_decode($json);
		 $opcion = $res->opcion;
		 $accion = $res->accion;
		 $datosSubidos = $res->parametros;

		 $conexion = \conectBdClases\conectBd::abrirConexion();

		 switch ($opcion) {

			 case 'producto':
		
				 $productoIni = new \productoClases\producto($datosSubidos);

				 switch ($accion) {
					 case 'modif':
						 $respuesta = $productoIni->modificar($conexion);
						 echo json_encode($respuesta);
					 break;

					 case 'borrar':
						 $respuesta = $productoIni->baja($conexion);
						 echo json_encode($respuesta);
					 break;
				
					 default:
						 throw new Exception('Error en Servidor');
					 break;
				 }
			
			 break;

			 case 'evento':
		
				 $datosSubidos->arrayParticipantes = inicilizarPati($conexion, $datosSubidos->arrayParticipantes);
				 $eventoIni = new \eventoClases\evento($datosSubidos);

				 switch ($accion) {
					 case 'modif':
						 $respuesta = $eventoIni->modificar($conexion);
						 echo json_encode($respuesta);
					 break;

					 case 'modifParti':
						 $respuesta = modificarPati($conexion,$eventoIni);
						 echo json_encode($respuesta);
					 break;
				
					 default:
						 throw new Exception('Error en Servidor');
					 break;
				 }
			
			 break;

			 case 'tienda':
	
				 $tiendaIni = new \tiendaClases\tienda($datosSubidos);

				 switch ($accion) {
					 case 'anhadir':
						 $respuesta = anhadirTienda($conexion,$tiendaIni);
						 echo json_encode($respuesta);
					 break;

					 case 'modif':
						 $respuesta = modifiTienda($conexion,$tiendaIni);
						 echo json_encode($respuesta);
					 break;
				
					 default:
						 throw new Exception('Error en Servidor');
					 break;
				 }
			
			 break;
		
			 default:
				 throw new Exception('Error en Servidor');
			 break;
		 }
	 }

}

catch (PDOException $e) {
	
		$conexion->rollBack();

		$resp = array(
					"estado"=>"fallo",
					"tipoError"=>$e->getMessage(),
					);

		echo json_encode($resp);
}

catch (Exception $e) {

		$resp = array(
					"estado"=>"fallo",
					"tipoError"=>$e->getMessage(),
					);

		echo json_encode($resp);
}

finally {
	
	$conexion = null;
}

function inicilizarPati($conexion, $arrayParti) {
	$resultado = array();

	foreach ($arrayParti as $datosParticipante) {
		$partiIni = new \participanteClases\participante($datosParticipante);
		$resultado[] = $partiIni;
	}

	return $resultado;
}

function modificarPati($conexion, $evento) {
	$datos = $evento->jsonSerialize();

	$conexion->beginTransaction();

	$tEvPar = $conexion->query("SELECT * from eventoParticipante where idEvento=".$datos["id"])->fetchAll();

	$aux = false;
	foreach ($tEvPar as $tablaSelect) {
		$aux = false;
		foreach ($datos["arrayParticipantes"] as $objetoSelect) {
			if(intval($tablaSelect["idParticipante"]) == intval($objetoSelect->jsonSerialize()["id"])) {
				$aux = true;
			}
		}

		if (!$aux) {
			\consultasClases\consultas::hacerDelete($conexion, "eventoParticipante", " where idParticipante=?", array($tablaSelect["idParticipante"]));
		}
	}

	foreach ($datos["arrayParticipantes"] as $objetoSelect) {
		$aux = false;
		foreach ($tEvPar as $tablaSelect) {
			if(intval($tablaSelect["idParticipante"]) == intval($objetoSelect->jsonSerialize()["id"])) {
				$aux = true;
			}
		}

		if (!$aux) {
			\consultasClases\consultas::hacerInsert($conexion, "eventoParticipante", array($datos["id"],intval($objetoSelect->jsonSerialize()["id"])));
		}
	}

	$conexion->commit();

	return array('estado'=>"ok",'mens'=>"Lista Participantes editada correctamente");
}


function anhadirTienda($conexion,$tienda) {

	$datos = $tienda->jsonSerialize();	

	\consultasClases\consultas::hacerInsert($conexion, "tienda", array(null,$datos["nombre"],$datos["direccion"],$datos["ciudad"],$datos["codigoPostal"],$datos["telefono"],$datos["email"],$datos["fax"]));

	return array('estado'=>"ok",'mens'=>"Tienda añadida correctamente");
}

function modifiTienda($conexion,$tienda) {

	$datos = $tienda->jsonSerialize();

	\consultasClases\consultas::hacerUpdate($conexion, "tienda", array("nombre","direccion","ciudad","codigoPostal","telefono","email","fax"), array($datos["nombre"],$datos["direccion"],$datos["ciudad"],$datos["codigoPostal"],$datos["telefono"],$datos["email"],$datos["fax"],$datos["id"]), "where id=?");	

	return array('estado'=>"ok",'mens'=>"Tienda modificada correctamente");
}

?>