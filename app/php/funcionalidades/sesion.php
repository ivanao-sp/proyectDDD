<?php

// echo password_hash("renaido2222",PASSWORD_BCRYPT);

require '../clases/conectBd.php';
require '../clases/consultas.php';
require '../clases/persona.php';
require '../interfaces/metodos.php';
require '../traits/metodos.php';
require '../clases/usuario.php';

try {

	$json = file_get_contents('php://input');
	$res = json_decode($json);

	$opcion = $res->opcion;

	$datosSubidos = $res->parametros;

	switch ($opcion) {

		case 'iniciar':

			$conexion = \conectBdClases\conectBd::abrirConexion();

			$respuesta = iniciarSesion($conexion,$datosSubidos);

			if (!$respuesta) {

				throw new Exception('correo o contraseña incorrectos');

			} else {

				$resp = array(
					"estado"=>"ok",
					"sesionn"=>$respuesta,
					);

				$conexion = null;

				echo json_encode($resp); 
			 }

		break;

		case 'cerrar':

			$respuesta = cerrarSesion();

			if($respuesta){

				$resp = array("estado"=>"ok");

				$conexion = null;

				echo json_encode($resp); 
			}

		break;

		case 'registrar':

			$respuesta = registrarUser($datosSubidos);

			if($respuesta){

				$resp = array('estado'=>"ok",'mens'=>"Registro realizado correctamente");

				$conexion = null;

				echo json_encode($resp); 
			}

		break;

		case 'modificar':

			$respuesta = modDatos($datosSubidos);

		break;
		
		default:

			throw new Exception('Error en Servidor');

		break;
	}

}

catch (PDOException $e) {
	
		$resp = array(
					"estado"=>"fallo",
					"tipoError"=>$e->getMessage(),
					);

		$conexion = null;

		echo json_encode($resp);
}

catch (Exception $e) {

		$resp = array(
					"estado"=>"fallo",
					"tipoError"=>$e->getMessage(),
					);

		$conexion = null;

		echo json_encode($resp);
}

function iniciarSesion($conexion, $credenciales) {				
	
	if (!isset($_SESSION['id'])) {

		$usuario = \consultasClases\consultas::hacerSelect($conexion, "*", "usuario", " where email=?", array($credenciales->e));

		if(count($usuario) == 0){
			return false;
		}

		$userDatos = array();
		$userDatos['id'] = $usuario[0]['id'];
		$userDatos['nom'] = $usuario[0]['nombre'];
		$userDatos['nif'] = $usuario[0]['NIF'];
		$userDatos['ape1'] = $usuario[0]['apellido1'];
		$userDatos['ape2'] = $usuario[0]['apellido2'];
		$userDatos['email'] = $usuario[0]['email'];
		$userDatos['tel'] = $usuario[0]['telefono'];
		$userDatos['dir'] = $usuario[0]['direccion'];
		$userDatos['local'] = $usuario[0]['localidad'];
		$userDatos['prov'] = $usuario[0]['provincia'];
		$userDatos['permisos'] = $usuario[0]['tipo'];
		$userDatos['fecha'] = $usuario[0]['fechaSesion'];
		$userDatos['logeado'] = "no";
		$userDatos['contrasenha'] = $usuario[0]['password'];
	}

	if (isset($userDatos['id'])) {

		if (password_verify($credenciales->p,$userDatos['contrasenha'])) {

			session_start();

			$_SESSION['id'] = $userDatos['id'];
			$_SESSION['nombre'] = $userDatos['nom'];
			$_SESSION['permisos'] = intval($userDatos['permisos']);
			$_SESSION['fecha'] = $userDatos['fecha'];

			$conexion = \conectBdClases\conectBd::abrirConexion();

			$updateFecha = 'UPDATE usuario SET fechaSesion = CURRENT_TIMESTAMP WHERE id = ?';
			$updateFechPrepa = $conexion->prepare($updateFecha);
			$updateFechPrepa->execute(array($_SESSION['id']));
			
			$userDatos['fecha'] = \consultasClases\consultas::hacerSelect($conexion, "fechaSesion", "usuario", " where email=?", array($credenciales->e))[0]["fechaSesion"];
			$userDatos['logeado'] = "si";


			return array_chunk($userDatos, 13, true)[0];
		} else {
			 return false;
		 }

	} else {
		return false;
	 }
}

function cerrarSesion() {

	session_start();

	session_destroy();

	return true;
}

function registrarUser($datos) {

	// IMPORTANTE: el usuario tipo anonimo solo debe tener permiso de hacer select en la mayoria de las tablas y permiso de insert solo sobre la tabla usuario para poder registrar un nuevo usuario, pero debido a que mi proveedor de hosting no me permite asignar permisos por tablas me veo obligado a establecer una conexion como tipo de usuario registrado y asi poder realizar el insert sin comprometer la seguridad de la aplicación.

		session_start();

		$_SESSION['permisos'] = 3;

	// -----------------------------
	
	$conexion = \conectBdClases\conectBd::abrirConexion();

	$arrayDatosUsuario = array();
	$arrayDatosUsuario["id"] = null;
	$arrayDatosUsuario["NIF"] = $datos->n;
	$arrayDatosUsuario["nombre"] = $datos->nom;
	$arrayDatosUsuario["apellido1"] = $datos->a1;
	$arrayDatosUsuario["apellido2"] = $datos->a2;
	$arrayDatosUsuario["telefono"] = $datos->t;
	$arrayDatosUsuario["email"] = $datos->e;
	$arrayDatosUsuario["direccion"] = $datos->d;
	$arrayDatosUsuario["localidad"] = $datos->l;
	$arrayDatosUsuario["provincia"] = $datos->p;
	$arrayDatosUsuario["password"] = $datos->c;
	$arrayDatosUsuario["tipoUsuario"] = 3;
	$arrayDatosUsuario["fechaSesion"] = "";

	$usuario = new \usuarioClases\usuario($arrayDatosUsuario);

	$datosUser = $usuario->jsonSerialize();

	\consultasClases\consultas::hacerInsert($conexion, "usuario", array(null,$datosUser["NIF"],$datosUser["nombre"],$datosUser["apellido1"],$datosUser["apellido2"],$datosUser["telefono"],$datosUser["email"],$datosUser["direccion"],$datosUser["localidad"],$datosUser["provincia"],password_hash($datos->c,PASSWORD_BCRYPT),3,""));

	return true;
}

function modDatos($datos) {

	session_start();
	$conexion = \conectBdClases\conectBd::abrirConexion();

	$arrayDatosUsuario = array();
	$arrayDatosUsuario["id"] = $datos->id;
	$arrayDatosUsuario["NIF"] = $datos->n;
	$arrayDatosUsuario["nombre"] = $datos->nom;
	$arrayDatosUsuario["apellido1"] = $datos->a1;
	$arrayDatosUsuario["apellido2"] = $datos->a2;
	$arrayDatosUsuario["telefono"] = $datos->t;
	$arrayDatosUsuario["email"] = $datos->e;
	$arrayDatosUsuario["direccion"] = $datos->d;
	$arrayDatosUsuario["localidad"] = $datos->l;
	$arrayDatosUsuario["provincia"] = $datos->p;


	if($datos->c == "") {
		$arrayDatosUsuario["password"] = \consultasClases\consultas::hacerSelect($conexion, "password", "usuario", " where id=?", array($datos->id))[0]["password"];
	} else {
		 $arrayDatosUsuario["password"] = password_hash($datos->c,PASSWORD_BCRYPT);
	 }

	$arrayDatosUsuario["tipoUsuario"] = \consultasClases\consultas::hacerSelect($conexion, "tipo", "usuario", " where id=?", array($datos->id))[0]["tipo"];

	$arrayDatosUsuario["fechaSesion"] = \consultasClases\consultas::hacerSelect($conexion, "fechaSesion", "usuario", " where id=?", array($datos->id))[0]["fechaSesion"];

	$usuario = new \usuarioClases\usuario($arrayDatosUsuario);

	echo json_encode($usuario->modificar($conexion));
}

?>