<?php namespace conectBdClases;

class conectBd extends \PDO {
	
	public static function abrirConexion() {

		try {

			$parametros = parse_ini_file('../usuBD/usuBD.ini',true);

			$bd = $parametros['bd']['baseD'];

			if(isset($_SESSION['permisos'])) {

				$usuario = $parametros[$_SESSION['permisos']]['user'];
				$contrasenha = $parametros[$_SESSION['permisos']]['passw'];

			} else {
		
				 $usuario = $parametros[4]['user'];
				 $contrasenha = $parametros[4]['passw'];
			 }	

			$conexion = new \PDO('mysql:host=localhost;port=82;dbname='.$bd.';charset=utf8', $usuario,$contrasenha);
			$conexion->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			return $conexion;
		}

		catch(PDOException $e) {
			return "Error en Conexión con Base de Datos: ".$e->getMessage();
		}
	}
}

?>