<?php namespace participanteClases;

class participante extends \personaClases\persona implements \JsonSerializable {
	
	private $grupo;

	public function __construct($arrayDatos){
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}
	
	public function jsonSerialize() {
		return array(
				"id" => $this->id,
				"NIF" => $this->NIF,
				"nombre" => $this->nombre,
				"apellido1" => $this->apellido1,
				"apellido2" => $this->apellido2,
				"telefono" => $this->telefono,
				"email" => $this->email,
				"direccion" => $this->direccion,
				"localidad" => $this->localidad,
				"provincia" => $this->provincia,
				"grupo" => $this->grupo
				);
	}

	public function modificarDatos($arrayDatos) {
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}
}

?>