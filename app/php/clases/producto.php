<?php namespace productoClases;

class producto extends \cosaClases\cosa implements \JsonSerializable, \metodosInterfaces\metodos {
	use \metodosTraits\metodos;

	private $categoria;
	private $arrayIma;
	private $idTienda;
	private $stock;
	private $estado;

	public function __construct($arrayDatos){
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}

	public function jsonSerialize() {
		// esta funcion nos permite serializar atributos privados y tambien la podemos usar para extraer todas las variables y trabajar con ellas en servidor
		return array(
				"id" => $this->id,
				"nombre" => $this->nombre,
				"descripcion" => $this->descripcion,
				"categoria" => $this->categoria,
				"fechaFinCampanha" => $this->fecha,
				"arrayIma" => $this->arrayIma,
				"idTienda" => $this->idTienda,
				"stock" => $this->stock,
				"precio" => $this->precio,
				"estado" => $this->estado
				);
	}

	public function modificarDatos($arrayDatos) {
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}
}

?>