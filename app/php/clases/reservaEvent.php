<?php namespace reservaEventClases;

class reservaEvent extends \reservaClases\reserva implements \JsonSerializable {
	private $idEvento;

	public function __construct($arrayDatos){
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}

	public function jsonSerialize() {
		return array(
				"idReserva" => $this->idReserva,
				"idUsuario" => $this->idUsuario,
				"idEvento" => $this->idEvento,
				"cantidad" => $this->cantidad,
				"precioTotal" => $this->precioTotal,
				"fecha" => $this->fecha
				);
	}

	public function modificarDatos($arrayDatos) {
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}
}

?>