<?php namespace tiendaClases;

class tienda implements \JsonSerializable {
	
	private $id;
	private $nombre;
	private $direccion;
	private $ciudad;
	private $codigoPostal;
	private $telefono;
	private $email;
	private $fax; 

	public function __construct($arrayDatos){
		foreach ($arrayDatos as $campo => $valor) {
		$this->$campo = $valor;
		}
	}

	public function jsonSerialize() {
		return array(
				"id" => $this->id,
				"nombre" => $this->nombre,
				"direccion" => $this->direccion,
				"ciudad" => $this->ciudad,
				"codigoPostal" => $this->codigoPostal,
				"telefono" => $this->telefono,
				"email" => $this->email,
				"fax" => $this->fax
				);
	}

	public function modificarDatos($arrayDatos) {
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}
}

?>