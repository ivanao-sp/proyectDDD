<?php namespace reservaProdClases;

class reservaProd extends \reservaClases\reserva implements \JsonSerializable {
	private $idProducto;
	private $idTienda;

	public function __construct($arrayDatos){
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}

	public function jsonSerialize() {
		return array(
				"idReserva" => $this->idReserva,
				"idUsuario" => $this->idUsuario,
				"idProducto" => $this->idProducto,
				"idTienda" => $this->idTienda,
				"cantidad" => $this->cantidad,
				"precioTotal" => $this->precioTotal,
				"fecha" => $this->fecha
				);
	}

	public function modificarDatos($arrayDatos) {
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}
}

?>