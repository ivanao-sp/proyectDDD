<?php namespace usuarioClases;

class usuario extends \personaClases\persona implements \JsonSerializable, \metodosInterfaces\metodos {
	use \metodosTraits\metodos;
	
	private $password; 
	private $tipoUsuario;

	public function __construct($arrayDatos){
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}

	public function jsonSerialize() {
		return array(
				"id" => $this->id,
				"NIF" => $this->NIF,
				"nombre" => $this->nombre,
				"apellido1" => $this->apellido1,
				"apellido2" => $this->apellido2,
				"telefono" => $this->telefono,
				"email" => $this->email,
				"direccion" => $this->direccion,
				"localidad" => $this->localidad,
				"provincia" => $this->provincia,
				"password" => $this->password,
				"tipoUsuario" => $this->tipoUsuario,
				"fechaSesion" => $this->fechaSesion
				);
	}

}	

?>