<?php namespace consultasClases;

class consultas {

	public static function hacerSelect($conexion, $campos, $tabla, $condicion, $datoCondicion) {

		$consulta = "SELECT ".$campos." from ".$tabla.$condicion;
		$consultaPrepa = $conexion->prepare($consulta);
	  $consultaPrepa->execute($datoCondicion);

  	return $consultaPrepa->fetchAll();
  }
	
	public static function hacerInsert($conexion, $tabla, $arrayValores) {

  	$numParametos="";
  	for ($i=0; $i < count($arrayValores) ; $i++) {
  		if($i <count($arrayValores)-1){
  			$numParametos.="?,";
   		} else {
  			 $numParametos.="?";
  	   } 
  				
  	}

  	$consulta = "INSERT into ".$tabla." values(".$numParametos.")";
		$consultaPrepa = $conexion->prepare($consulta);
	  $consultaPrepa->execute($arrayValores);

  }

  public static function hacerDelete($conexion, $tabla, $condicion, $datoCondicion) {

  	$consulta = "DELETE from ".$tabla.$condicion;
		$consultaPrepa = $conexion->prepare($consulta);
	   $consultaPrepa->execute($datoCondicion);

  }

  public static function hacerUpdate($conexion, $tabla, $arrayCampos, $arrayValores, $condicion) {

  	$cambios="";
  	$keys = array_keys($arrayCampos);

  	foreach ($arrayCampos as $clave => $valor) {
  		if($clave == end($keys)){
  			$cambios.=$valor."=?";
  		} else {
  			 $cambios.=$valor."=?,";
  		 }
  	}

  	$consulta = "UPDATE ".$tabla." SET ".$cambios.$condicion;
		$consultaPrepa = $conexion->prepare($consulta);
	  $consultaPrepa->execute($arrayValores);
	}
}


?>