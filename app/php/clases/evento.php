<?php namespace eventoClases;

class evento extends \cosaClases\cosa implements \JsonSerializable, \metodosInterfaces\metodos {
	use \metodosTraits\metodos;
	
	private $rutaImagen;
	private $lugar;
	private $fechasPosibles;
	private $aforo;
	private $entradasDisponibles;
	private $arrayParticipantes;

	public function __construct($arrayDatos){
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}

	public function jsonSerialize() {
		return array(
				"id" => $this->id,
				"nombre" => $this->nombre,
				"descripcion" => $this->descripcion,
				"rutaImagen" => $this->rutaImagen,
				"lugar" => $this->lugar,
				"fecha" => $this->fecha,
				"fechasPosibles" => $this->fechasPosibles,
				"aforo" => $this->aforo,
				"precioEntrada" => $this->precio,
				"entradasDisponibles" => $this->entradasDisponibles,
				"arrayParticipantes" => $this->arrayParticipantes
				);
	}

	public function modificarDatos($arrayDatos) {
		foreach ($arrayDatos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}
}

?>
